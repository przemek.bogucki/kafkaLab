set KAFKA_SOURCE (cat kafkaSource)
set KAFKA_MD5_SOURCE (cat kafkaMD5Source)
set KAFKA_VERSION (cat kafkaVersion)
set DOCKER_TAG ('kafka_'+"$KAFKA_VERSION")
set scriptpath ($MyInvocation.MyCommand.Path)
set dir (Split-Path $scriptpath)

set command "cd ""$DIR"" ; Docker build --build-arg KAFKA_SOURCE=""$KAFKA_SOURCE"" --build-arg KAFKA_MD5_SOURCE=""$KAFKA_MD5_SOURCE"" --build-arg KAFKA_VERSION=""$KAFKA_VERSION"" --pull -t ""$DOCKER_TAG"" ."


echo $command

Write-Host -NoNewLine 'Press any key to continue...';
$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

Start-Process -WorkingDirectory "D:\development\kafkaLab\kafkaDocker" -Verb runas -ArgumentList "-NoExit -Command $command " powershell 