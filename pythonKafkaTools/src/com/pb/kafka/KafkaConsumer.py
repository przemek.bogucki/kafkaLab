import argparse
import kafka
from kafka import KafkaConsumer


CONF_TOPIC_ID = "topicID"
CONF_GROUP_ID = "groupID"
CONF_TIME_TO_SLEEP = "timeToSleep"
CONF_BOOTSTRAP_STRING = "127.0.0.1:9091,127.0.0.1:9092,127.0.0.1:9093"


def start_consumer(input_config):
    kafka_consumer = KafkaConsumer(enable_auto_commit="false", group_id=input_config.get(CONF_GROUP_ID),
                                   bootstrap_servers=CONF_BOOTSTRAP_STRING)
    topic_name = input_config.get(CONF_TOPIC_ID)
    kafka_consumer.subscribe(topics=topic_name, listener=LocalConsumerRebalanceListener())
    while True:
        records = kafka_consumer.poll(2000, 10)
        if len(records) > 0:
            for record in records.values():
                print(f"C: {record}")
        else:
            print("C: No records")
        kafka_consumer.commit()


class LocalConsumerRebalanceListener(kafka.ConsumerRebalanceListener):
    def on_partitions_revoked(self, revoked):
        print(f"revoked: {revoked}")

    def on_partitions_assigned(self, assigned):
        print(f"assigned: {assigned}")


def input_parser():
    parser = argparse.ArgumentParser(description="Available input parameters",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-tid", "--topicID", help="ID of Kafka Topic", default="testTopic")
    group_id_param = f'--{CONF_GROUP_ID}'
    parser.add_argument(f"-gid", group_id_param, help="ID of client group", default="testGroup")
    args = parser.parse_args()
    input_config = vars(args)
    print(f"input parameters:{input_config}")
    return input_config


if __name__ == '__main__':
    config = input_parser()
    start_consumer(config)
