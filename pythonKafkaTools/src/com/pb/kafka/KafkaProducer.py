import argparse
import time

from kafka import KafkaProducer
from kafka.producer.future import RecordMetadata

CONF_TOPIC_NAME = "topicID"
CONF_NUMER_OF_MESSAGES = "numberOfMessages"
CONF_TIME_TO_SLEEP = "timeToSleep"
CONF_BOOTSTRAP_STRING = '127.0.0.1:9091,127.0.0.1:9092,127.0.0.1:9093'


def start_producer(input_config):
    print('Kafka Test Producer')
    producer = KafkaProducer(bootstrap_servers=CONF_BOOTSTRAP_STRING, request_timeout_ms=5000)
    i = 0
    up_to = int(input_config.get(CONF_NUMER_OF_MESSAGES))
    while i < up_to:
        value = f"Message id {i}"
        key = str(i % 10)
        producer.send(topic=input_config.get(CONF_TOPIC_NAME), value=value.encode(), key=key.encode(),
                      timestamp_ms=round(time.time()*1000)).\
            add_callback(callback_producer, key=key, value=value).\
            add_errback(callback_error, key=key, value=value)
        i = i + 1
        time.sleep(int(input_config.get(CONF_TIME_TO_SLEEP))/1000)


def callback_producer(record_metadate: RecordMetadata, **params):
    print(f"P: success sent: key={params.get('key')} , value='{params.get('value')}' - metadate:{record_metadate}")


def callback_error(err, **params):
    print(f"P: error for: key={params.get('key')} , value='{params.get('value')}' - Error: {err}")


def input_parser():
    parser = argparse.ArgumentParser(description="Available input parameters",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-tid", "--topicID", help="ID of Kafka Topic", default="testTopic")
    parser.add_argument("-mn", "--numberOfMessages", help="Number of messages to send", default=10000)
    parser.add_argument("-ts", "--timeToSleep", help="interval between messages in ms", default=500)
    args = parser.parse_args()
    input_config = vars(args)
    print(f"input parameters:{input_config}")
    return input_config


if __name__ == '__main__':
    config = input_parser()
    start_producer(config)
