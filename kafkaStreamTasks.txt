#Kafka - PowerShall version

#####################
##				   ##
##   Lab warm up   ##
#####################

Start Docker Desktop

#####################
##----- Lab 1  ----##
##  Cluster Setup  ##
#####################

execute setupAll.ps1

#---- Create cluster
open 127.0.0.1:9000 in the browser

1. open Kafka Manager(web) -> Cluster -> Add Cluster
2. setup
	Cluster Name = cl1
	Cluster Zookeeper Hosts = $MACHINE_IP:2181
	Enable JMX Polling = checked
	Poll consumer information (Not recommended for large # of consumers) = checked
	Keep other with default values
	Press "Save" on the bottom of the page

3. choose "Go to cluster view"

######################
##------ Lab 3 -----##
## Simple data flow ##
######################

#In the kafkaLab\KafkaTools

1. Run producer
java -jar .\KafkaTestDataProducer-1.3.jar

2. Run two consumers
java -jar .\KafkaStreamTestApp-1.3.jar
java -jar .\KafkaStreamTestApp-1.3.jar


#Check what data consumers receive
#Stop all clients (Ctrl-c)


######################
##------ Lab 4 -----##
##  Order data flow ##
######################

#In the kafkaLab\KafkaTools

1. Run order producer - check producer implementation
java -jar .\KafkaOrderProducer-1.3.jar

2. Run two consumers - check consumer implementation, check consumer Serde configuration
java -jar .\KafkaStreamOrderTransformationsApp-1.3.jar
java -jar .\KafkaStreamOrderTransformationsApp-1.3.jar

#Start conusmers on new topics
java -jar .\KafkaSimpleDataConsumer-1.3.jar false gr1 ProductType-1
java -jar .\KafkaSimpleDataConsumer-1.3.jar false gr1 ProductType-2
java -jar .\KafkaSimpleDataConsumer-1.3.jar false gr1 ProductType-rest


#Check what data consumers receive
#Stop all clients (Ctrl-c)

######################
##------ Lab 5 -----##
##     groupping    ##
######################

#In the kafkaLab\KafkaTools

1. Run test producer - check producer implementation
java -jar .\KafkaTestDataProducer-1.3.jar

2. Run table consumers - check =implementation, check HTTP output
java -jar .\KafkaTableTestApp-1.3.jar


######################
##------ Lab 6 -----##
##   table object   ##
######################

#In the kafkaLab\KafkaTools

1. Run test producer - check producer implementation
java -jar .\KafkaTestDataProducer-1.3.jar

2. Run table consumers - check implementation, check HTTP output
java -jar .\KafkaTableTestApp-1.3.jar

3. Run next table consumers -  check HTTP output
java -jar .\KafkaTableTestApp-1.3.jar

######################
##------ Lab 7 -----##
##    Aggregation   ##
######################

#In the kafkaLab\KafkaTools

1. Run test producer - check producer implementation
java -jar .\KafkaOrderProducer-1.3.jar

2. Run table consumers - check implementation
java -jar .\KafkaStreamOrderAggregationApp-1.3.jar

3. Run next table consumers
java -jar .\KafkaStreamOrderAggregationApp-1.3.jar

######################
##------ Lab 8 -----##
##      Joining     ##
######################

#In the kafkaLab\KafkaTools

1. Run test producer - check producer implementation
java -jar .\KafkaOrderProducer-1.3.jar
java -jar .\KafkaProductProducer-1.3.jar

2. Run table consumers - check implementation
java -jar .\KafkaJoinStreamApp-1.3.jar

3. Run next table consumers - check implementation
java -jar .\KafkaJoinTableApp-1.3.jar

########################
##------  Lab 9 ------##
##      Windowing     ##
########################

1. Run test producer - check producer implementation
java -jar .\KafkaOrderProducer-1.3.jar

2. Run table consumers - check implementation
java -jar .\KafkaStreamOrderWindowedApp-1.3.jar A
java -jar .\KafkaStreamOrderWindowedApp-1.3.jar B
java -jar .\KafkaStreamOrderWindowedApp-1.3.jar C

########################
##------  Lab 10  ----##
##Windowing order fix ##
########################
1. Run test producer - check producer implementation
java -jar .\KafkaOrderProducer-1.3.jar true

2. Run table consumers - check implementation
java -jar .\KafkaStreamOrderWindowedApp-1.3.jar D