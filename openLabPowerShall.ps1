set IP_INTERFACE_ALIAS 'Ethernet'  #It must be updated to reflect existing network interface
set IP (Get-NetIPConfiguration -InterfaceAlias $IP_INTERFACE_ALIAS).IPv4Address.IPAddress
set LOC (Get-Location).tostring();

if ($IP -match "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"){
	echo ("Found IP:" + $IP)
	set command "Set-Variable -Name ""MACHINE_IP"" -Value $IP -Scope global; Write-Output ""MACHINE_IP:"" ; Write-Output $IP ; cd $LOC"
	
	Start-Process powershell -WorkingDirectory "C:\" -Verb runas -ArgumentList "-NoExit -Command $command "
}else{
	echo "Network Interface" + $IP_INTERFACE_ALIAS + " not found. Check avaliable interfaces with ipconfig and update IP_INTERFACE_ALIAS value in this script"
	Write-Host -NoNewLine 'Press any key to continue...';
}