package com.pb.edu.kafka;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.kstream.Window;
import org.apache.kafka.streams.kstream.Windowed;

import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.models.Order;

public class KafkaUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSSS");

	public static void createCompactedTopic(KafkaBaseNetworkConfiguration inputConfiguration, String topicName)
			throws InterruptedException, ExecutionException {
		System.out.println("Create compacted topic:" + topicName);

		Properties props = new Properties();
		props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, inputConfiguration.getConfiguration());

		try (Admin admin = Admin.create(props)) {
			int partitions = 3;
			short replicationFactor = 3;

			Map<String, String> topicProperties = new HashMap<String, String>();

			topicProperties.put(TopicConfig.MIN_COMPACTION_LAG_MS_CONFIG, "500");
			topicProperties.put(TopicConfig.MAX_COMPACTION_LAG_MS_CONFIG, "1000");
			topicProperties.put(TopicConfig.CLEANUP_POLICY_CONFIG, TopicConfig.CLEANUP_POLICY_COMPACT);
			topicProperties.put(TopicConfig.DELETE_RETENTION_MS_CONFIG, "2000");
			topicProperties.put(TopicConfig.MIN_CLEANABLE_DIRTY_RATIO_CONFIG, "0.01");

			KafkaFuture<Set<String>> futureListOfTopics = admin.listTopics().names();
			Set<String> listOfTopics = futureListOfTopics.get();

			if (listOfTopics.contains(topicName)) {
				DeleteTopicsResult result = admin.deleteTopics(Collections.singleton(topicName));
				result.all().get();
			}

			CreateTopicsResult result = admin.createTopics(Collections
					.singleton(new NewTopic(topicName, partitions, replicationFactor).configs(topicProperties)));

			KafkaFuture<Void> future = result.values().get(topicName);
			future.get();

		}
	}

	public static String toString(ProducerRecord<String, ? extends Object> record, RecordMetadata metadata) {
		StringBuilder sb = new StringBuilder();
		sb.append("P key:" + record.key());
		sb.append(", value:'" + record.value() + "'");
		sb.append(", topic:" + metadata.topic());
		sb.append(", partition:" + metadata.partition());
		sb.append(", offset:" + metadata.offset() + "");
		sb.append(", headers:");
		addHeader(record.headers(), sb);
		return sb.toString();
	}

	public static String toString(String customerPrefix, ConsumerRecord<String, String> record) {
		StringBuilder sb = new StringBuilder();
		sb.append(customerPrefix);
		sb.append(" key:" + record.key());
		sb.append(", value:" + record.value());
		sb.append(", topic:" + record.topic());
		sb.append(" partition:" + record.partition());
		sb.append(" offset:" + record.offset());
		sb.append(", headers:");
		addHeader(record.headers(), sb);
		sb.append(", tType:" + record.timestampType());
		sb.append(", tvalue:" + record.timestamp());
		return sb.toString();
	}

	private static void addHeader(Headers headers, StringBuilder sb) {
		if (headers != null) {
			Iterator<Header> iterator = headers.iterator();
			while (iterator.hasNext()) {
				Header h = iterator.next();
				byte[] bytes = h.value();
				String key = h.key();
				sb.append("{K:" + key + ", V:" + bytes + "}");
				if (iterator.hasNext())
					sb.append(",");
			}
		}
	}

	public static String toTimeString(List<Order> orders) {
		StringBuilder sb = new StringBuilder();
		for (Order o : orders) {
			sb.append("OrderId:" + o.getOrderId() + " OrderTime:" + KafkaUtils.timeToString(o.getOrderTime()) + " ");
		}
		return sb.toString();
	}

	public static String toString(Windowed<?> windowKey) {
		return "key:" + windowKey.key() + " " + toString(windowKey.window());

	}

	public static String toString(Window window) {
		return timeToString(window.start()) + "-" + timeToString(window.end()) + " range "
				+ (window.end() - window.start()) + "ms";

	}

	public static String timeToString(long epoch) {
		return sdf.format(new Date(epoch));
	}

}
