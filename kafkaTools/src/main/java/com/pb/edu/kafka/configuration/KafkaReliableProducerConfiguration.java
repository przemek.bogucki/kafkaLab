package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaReliableProducerConfiguration extends KafkaBaseProducerConfiguration {
	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration,
			KafkaReliableProducerPolicy kafkaReliableProducerSendPolicy) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.ACKS_CONFIG, Integer.toString(kafkaReliableProducerSendPolicy.getAckNumber()));
		return props;
	}
}
