package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClientConfig;

public class KafkaBaseConsumerConfiguration {
	public static Properties getBaseProperties(KafkaBaseNetworkConfiguration inputConfiguration) {
		Properties props = new Properties();
		props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, inputConfiguration.getConfiguration());
		return props;
	}
}
