package com.pb.edu.kafka.configuration.streams;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import com.pb.edu.kafka.configuration.KafkaBaseConsumerConfiguration;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;

public class KafkaStreamTestConsumerConfiguration extends KafkaBaseConsumerConfiguration {
	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration,
			KafkaStreamTestConsumerPolicy consumerConfiguration) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerConfiguration.getConsumerGroup());
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "testTopicStreamConsumer");
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 2);

		return props;
	}
}
