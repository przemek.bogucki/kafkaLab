package com.pb.edu.kafka.models;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectSerializer<E> implements Serializer<E> {

	private static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public byte[] serialize(String topic, E object) {
		byte[] retVal = null;

		try {
			retVal = objectMapper.writeValueAsString(object).getBytes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
}
