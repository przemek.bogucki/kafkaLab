package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.producer.ProducerConfig;

public class KafkaBaseProducerConfiguration {
	public static Properties getBaseProperties(KafkaBaseNetworkConfiguration inputConfiguration) {
		Properties props = new Properties();
		props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, inputConfiguration.getConfiguration());
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, 2);
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, 32);
		props.put(ProducerConfig.LINGER_MS_CONFIG, 5);
		props.put(ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG, 3000);
		props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 1000);

		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
		return props;

	}
}
