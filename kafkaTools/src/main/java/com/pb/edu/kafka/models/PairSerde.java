package com.pb.edu.kafka.models;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PairSerde implements Serde<Pair<Long, Long>> {

	@Override
	public Serializer<Pair<Long, Long>> serializer() {
		return new Serializer<Pair<Long, Long>>() {
			@Override
			public byte[] serialize(String topic, Pair<Long, Long> pair) {
				byte[] retVal = null;
				ObjectMapper objectMapper = new ObjectMapper();
				try {
					retVal = objectMapper.writeValueAsString(pair.getLeft() + ":" + pair.getRight()).getBytes();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return retVal;
			}
		};
	}

	@Override
	public Deserializer<Pair<Long, Long>> deserializer() {
		return new Deserializer<Pair<Long, Long>>() {
			@Override
			public Pair<Long, Long> deserialize(String topic, byte[] data) {
				ObjectMapper mapper = new ObjectMapper();
				Pair<Long, Long> pair = null;
				try {
					String val = mapper.readValue(data, String.class);
					String[] vals = val.split(":");
					pair = Pair.of(Long.valueOf(vals[0]), Long.valueOf(vals[1]));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return pair;
			}
		};
	}

}
