package com.pb.edu.kafka.configuration;

public class KafkaReliableProducerPolicy extends KafkaTestProducerPolicy {

	private int ackNumber = -1;

	public int getAckNumber() {
		return ackNumber;
	}

	public void setAckNumber(int ackNumber) {
		this.ackNumber = ackNumber;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	private static String getHelpMessage() {
		return "Expected: ack factor(as int), topicName (as String)";
	}

	public static KafkaReliableProducerPolicy parseFromInputArgs(String[] args) {
		KafkaReliableProducerPolicy sendConfiguration = new KafkaReliableProducerPolicy();
		if (args.length > 0) {
			try {

				if (args.length > 0) {
					sendConfiguration.setAckNumber(Integer.parseInt(args[0]));
				}
				if (args.length > 1) {
					sendConfiguration.setTopicName(args[1]);
				}
			} catch (NumberFormatException nfe) {
				throw new IllegalArgumentException("Incorrect input format. " + getHelpMessage());
			}

		}
		return sendConfiguration;
	}

	@Override
	public String toString() {
		return "KafkaReliableProducerPolicy [ackNumber=" + ackNumber + ", topicName=" + getTopicName()
				+ ", keyPrefix=" + getKeyPrefix() + ", messagesToSend=" + getMessagesToSend() + ", keyModule="
				+ getKeyModule() + ", sendSleep=" + getSendSleep() + "]";
	}

}
