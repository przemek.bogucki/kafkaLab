package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.Stores;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamJoinConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamJoinConsumerPolicy;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerPolicy;
import com.pb.edu.kafka.models.Order;
import com.pb.edu.kafka.models.OrderSerde;
import com.pb.edu.kafka.models.Product;
import com.pb.edu.kafka.models.ProductSerde;

public class KafkaJoinTableApp {
	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamJoinConsumerPolicy consumerPolicy = KafkaStreamJoinConsumerPolicy.parseFromArgsInput(args);

		consumerPolicy.setConsumerGroup(consumerPolicy.getConsumerGroup());

		KafkaJoinTableApp consumer = new KafkaJoinTableApp();

		Properties configuration = KafkaStreamJoinConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka Join Table Order and Product");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				consumer.latch.countDown();
				consumer.streams.close();
			}
		});

		consumer.work(configuration, consumerPolicy);
	}

	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);

	private void work(Properties configuration, KafkaStreamJoinConsumerPolicy consumerPolicy) throws IOException {
		// Initialization of the builder
		final StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create the source stream from the given topic
		KTable<String, Order> theOrderTable = builder
				.stream(consumerPolicy.getOrderTopicName(), Consumed.with(Serdes.String(), OrderSerde.getOrderSerde()))
				.toTable();

		KTable<String, Product> theProductTable = builder.table(consumerPolicy.getProductTopicName(),
				Consumed.with(Serdes.String(), ProductSerde.getProductSerde()));

		KeyValueBytesStoreSupplier inMemoryValueStore = Stores.inMemoryKeyValueStore("JoinStorage");
		Materialized<String, String, KeyValueStore<Bytes, byte[]>> materialized = Materialized.as(inMemoryValueStore);
		materialized.withKeySerde(Serdes.String()).withValueSerde(Serdes.String());

		theOrderTable
				.join(theProductTable, (o) -> "productId:" + o.getProductId(),
						(o, p) -> " At: " + KafkaUtils.timeToString(p.getCreationTime()) + " order:" + o.getOrderId()
								+ ", product:" + o.getProductId() + ", current price:" + p.getProductPrice(),
						materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});
		System.out.println(topology.describe());

		// go Go GO
		try {
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialisation and to get
			// already initialised local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}
			System.out.println("Stream state:" + streams.state());

			ReadOnlyKeyValueStore<String, String> keyValueStore = streams
					.store(StoreQueryParameters.fromNameAndType("JoinStorage", QueryableStoreTypes.keyValueStore()));
			while (latch.getCount() > 0) {
				try {
					System.out.println("Table state at:" + System.currentTimeMillis());
					keyValueStore.all().forEachRemaining(
							kv -> System.out.println("Storage content C key:" + kv.key + " , msg:" + kv.value));
					Thread.sleep(2500);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
