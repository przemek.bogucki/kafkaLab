package com.pb.edu.kafka.consumers;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestConsumerConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestConsumerPolicy;

public class KafkaControlledDataConsumer {
	private CountDownLatch latch = new CountDownLatch(1);
	private AtomicBoolean paused = new AtomicBoolean(false);
	private AtomicBoolean poll = new AtomicBoolean(true);
	private volatile RequestedAction requestedAction = RequestedAction.NO_ACTION;
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	private enum RequestedAction {
		PAUSE_BEFORE_COMMIT('p'), PAUSE_AFTER_COMMIT('t'), STOP_POLLING('s'), RESUME('r'), HELP('?'), NO_ACTION(' ');

		private char inputChar;

		RequestedAction(char inputChar) {
			this.inputChar = inputChar;
		}

		public char getInputChar() {
			return inputChar;
		}

		public static String getControllLegend() {
			StringBuilder sb = new StringBuilder();
			sb.append("'" + STOP_POLLING.getInputChar() + "' - Stop consumer poll\n");
			sb.append("'" + PAUSE_BEFORE_COMMIT.getInputChar() + "' - Pause consumer before commit\n");
			sb.append("'" + PAUSE_AFTER_COMMIT.getInputChar() + "' - Pause consumer after commit before poll()\n");
			sb.append("'" + RESUME.getInputChar() + " resume to regular work\n");
			sb.append("'" + HELP.getInputChar() + " print this help");
			return sb.toString();
		}
	}

	private KafkaConsumer<String, String> consumer;

	/**
	 * 
	 * @param args client id, topic name
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		// Process passed arguments to validate and display help message
		if (KafkaTestConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		// Get default network configuration - list of Kafka nodes
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();
		// Get consumer policy based on defaults and input parameters
		KafkaTestConsumerPolicy consumerPolicy = KafkaTestConsumerPolicy.parseFromArgsInput(args);
		// create properties ready to pass to Kafka consumer
		Properties configuration = KafkaTestConsumerConfiguration.getProperties(networkConfiguration, consumerPolicy);

		System.out.println("Start Kafka Contolled Data Consumer");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		System.out.println(RequestedAction.getControllLegend());
		System.out.println("press to start...");
		System.in.read();

		// Create consumer - this class
		KafkaControlledDataConsumer kafkaControlledDataConsumer = new KafkaControlledDataConsumer();

		// Start main processing
		kafkaControlledDataConsumer.work(configuration, consumerPolicy);
	}

	private void work(Properties configuration, KafkaTestConsumerPolicy consumerPolicy) {
		// register Ctrl-C handler
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				latch.countDown();
			}
		});

		runStInRead();

		// Create Kafka consumer, pass configuration parameters
		consumer = new KafkaConsumer<>(configuration);

		// Connect to the topic and register handler for partitions assignments updates
		consumer.subscribe(Arrays.asList(consumerPolicy.getTopicName()), new ConsumerRebalancer());

		scheduler.schedule(() -> pollAndPrint(), 10, TimeUnit.MILLISECONDS);

	}

	private void pollAndPrint() {
		if (latch.getCount() == 0) {
			return;
		}
		if (poll.get()) {
			if (paused.get()) {
				System.out.println("Paused at:" + consumer.paused() + " press '" + RequestedAction.RESUME.getInputChar()
						+ "' to resume");
			}
			// Get records, wait 4sec to get something then end
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
			// Print the content
			if (!records.isEmpty()) {
				records.forEach(record -> {
					System.out.println(KafkaUtils.toString("CCtrl", record));
				});
			} else {
				System.out.println("CCtrl no new data");
			}
			if (requestedAction == RequestedAction.PAUSE_BEFORE_COMMIT) {
				pause();
			}
			// Commit the offset in synchronous mode
			consumer.commitSync();

			if (requestedAction == RequestedAction.PAUSE_AFTER_COMMIT) {
				pause();
			}
		} else {
			System.out.println("POLL Stopped, press '" + RequestedAction.RESUME.getInputChar() + "' to resume");
		}
		if (requestedAction == RequestedAction.RESUME) {
			resume();
		}
		scheduler.schedule(() -> pollAndPrint(), 1000, TimeUnit.MILLISECONDS);
	}

	private void resume() {
		paused.set(false);
		poll.set(true);
		requestedAction = RequestedAction.NO_ACTION;
		consumer.resume(consumer.paused());
		System.out.println("RESUMED");
	}

	private void pause() {
		consumer.pause(consumer.assignment());
		paused.set(true);
		requestedAction = RequestedAction.NO_ACTION;
		System.out.println("PAUSING at:" + consumer.paused());
	}

	private void runStInRead() {
		Thread stIn = new Thread() {
			@Override
			public void run() {
				while (latch.getCount() > 0) {
					try {
						int input = Character.toLowerCase(System.in.read());
						if (input == RequestedAction.PAUSE_BEFORE_COMMIT.getInputChar()) {
							if (paused.get()) {
								requestedAction = RequestedAction.RESUME;
							} else {
								System.out.println("PAUSED before Commit requested");
								requestedAction = RequestedAction.PAUSE_BEFORE_COMMIT;
							}
						} else if (input == RequestedAction.STOP_POLLING.getInputChar()) {
							if (poll.get()) {
								poll.set(false);
								System.out.println("POLL Stopped");
							} else {
								requestedAction = RequestedAction.RESUME;
								System.out.println("Resume requested");
							}
						} else if (input == RequestedAction.PAUSE_AFTER_COMMIT.getInputChar()) {
							if (paused.get()) {
								requestedAction = RequestedAction.RESUME;
							} else {
								System.out.println("PAUSED after Poll Commit requested");
								requestedAction = RequestedAction.PAUSE_AFTER_COMMIT;
							}
						} else if (input == RequestedAction.RESUME.getInputChar()) {
							requestedAction = RequestedAction.RESUME;
							System.out.println("Resume requested");
						} else if (input == RequestedAction.HELP.getInputChar()) {
							System.out.println(RequestedAction.getControllLegend());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			};
		};
		stIn.start();
	}

	private class ConsumerRebalancer implements ConsumerRebalanceListener {
		@Override
		public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
			System.out.println("Repartition rebalance detected. Current state:" + partitions + " Paused status:"
					+ consumer.paused());
		}

		@Override
		public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
			System.out
					.println("Repartition new assigen detected:" + partitions + " Paused status:" + consumer.paused());
		}
	}
}
