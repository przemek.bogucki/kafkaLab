package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamTestConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamTestConsumerPolicy;

public class KafkaStreamTestApp {

	public static void main(String[] args) throws IOException {
		// Process passed arguments to validate and display help message
		if (KafkaStreamTestConsumerPolicy.handleHelpRequest(args)) {
			return;
		}

		// Get default network configuration - list of Kafka nodes
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();
		// Get consumer policy based on defaults and input parameters
		KafkaStreamTestConsumerPolicy consumerPolicy = KafkaStreamTestConsumerPolicy.parseFromArgsInput(args);
		// create properties ready to pass to Kafka consumer
		Properties configuration = KafkaStreamTestConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka Stream Consumer");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// Create consumer - this class
		KafkaStreamTestApp consumer = new KafkaStreamTestApp();
		// Start processing
		consumer.work(configuration, consumerPolicy);
	}

	private void work(Properties configuration, KafkaStreamTestConsumerPolicy consumerPolicy) throws IOException {
		CountDownLatch latch = new CountDownLatch(1);

		// Create stream builder
		StreamsBuilder builder = new StreamsBuilder();

		// The main stream connected to the topic
		KStream<String, String> theStream = builder.stream(consumerPolicy.getTopicName());

		// add processing for each received event, simple print
		theStream.foreach((k, v) -> {
			System.out.println("Stream processing C key:" + k + " , msg:" + v);
		});

		// Create topology based on the stream defined in the builder
		Topology topology = builder.build();

		// create Kafka Stream, pass Kafka ready configuration
		KafkaStreams streams = new KafkaStreams(topology, configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				streams.close();
				latch.countDown();
			}
		});

		// go Go GG
		try {
			streams.start();
			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
