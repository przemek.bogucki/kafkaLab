package com.pb.edu.kafka.producers;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaProductProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaProductProducerPolicy;
import com.pb.edu.kafka.models.Product;

public class KafkaProductProducer {
	public static void main(String[] args) {

		if (KafkaProductProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		KafkaProductProducerPolicy sendPolicy = KafkaProductProducerPolicy.parseFromInputArgs(args);
		KafkaProductProducer producer = new KafkaProductProducer();

		Properties configuration = KafkaProductProducerConfiguration.getProperties(inputConfiguration);

		System.out.println("Start Kafka Product Producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		producer.work(configuration, sendPolicy);

	}

	public void work(Properties configuration, KafkaProductProducerPolicy sendPolicy) {
		Producer<String, Product> producer = new KafkaProducer<>(configuration);

		System.out.println(producer.partitionsFor(sendPolicy.getProductTopicName()));

		int i = 0;

		ThreadLocalRandom random = ThreadLocalRandom.current();

		while (sendPolicy.getMessagesToSend() == 0 || i < sendPolicy.getMessagesToSend()) {
			try {

				long orderTime = sendPolicy.isRandomTime()
						? System.currentTimeMillis() + (RandomUtils.nextLong(0, 60000) - 30000)
						: System.currentTimeMillis();

				int productId = random.nextInt(1, sendPolicy.getProductIdRange());
				int price = random.nextInt(10, 100);

				Product product = new Product(productId, "Product_" + productId, price, orderTime);

				String key = sendPolicy.getProductKeyPrefix() + product.getProductId();
				ProducerRecord<String, Product> record = new ProducerRecord<String, Product>(
						sendPolicy.getProductTopicName(), null, product.getCreationTime(), key, product);

				record.headers().add("Partition", null);

				producer.send(record, new Callback() {

					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception == null) {
							System.out.println(KafkaUtils.toString(record, metadata));
						} else {
							System.out.println(key + " , " + product + " - ERROR:\n");
							exception.printStackTrace();
						}

					}
				});
				Thread.sleep(sendPolicy.getSendSleep());
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
			i++;
		}
		producer.flush();
		producer.close();
		System.out.println("Push done");

	}
}
