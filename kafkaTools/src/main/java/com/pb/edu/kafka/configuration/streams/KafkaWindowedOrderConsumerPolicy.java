package com.pb.edu.kafka.configuration.streams;

import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.SlidingWindows;
import org.apache.kafka.streams.kstream.TimeWindows;

public class KafkaWindowedOrderConsumerPolicy extends KafkaStreamOrderConsumerPolicy {
	private Type type = Type.A;

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	protected static String getHelpMessage() {
		return "Expected:Type [A,B,C,D] , Consumer Group  Id [as string], Topic Name [as string]";
	}

	public static KafkaWindowedOrderConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaWindowedOrderConsumerPolicy consumerConsumingPolicy = new KafkaWindowedOrderConsumerPolicy();
		if (args.length > 0) {
			Type type;
			try {
				type = Type.valueOf(args[0]);
			} catch (IllegalArgumentException i) {
				type = Type.A;
			}
			consumerConsumingPolicy.setType(type);
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setConsumerGroup(args[1]);
		}
		if (args.length > 2) {
			consumerConsumingPolicy.setTopicName(args[2]);
		}
		return consumerConsumingPolicy;
	}

	public enum Type {
		A(TimeWindows.class.getCanonicalName()), B(SessionWindows.class.getCanonicalName()),
		C(SlidingWindows.class.getCanonicalName()), D("Test version"), E("Secret version");

		private final String widowingType;

		Type(String widowingType) {
			this.widowingType = widowingType;
		}

		public String getWidowingType() {
			return widowingType;
		}

	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}
}
