package com.pb.edu.kafka.consumers;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestConsumerConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestConsumerPolicy;

public class KafkaTestDataConsumer {
	private CountDownLatch latch = new CountDownLatch(1);

	/**
	 * 
	 * @param args client id, topic name
	 */
	public static void main(String[] args) {

		// Process passed arguments to validate and display help message
		if (KafkaTestConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		// Get default network configuration - list of Kafka nodes
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();
		// Get consumer policy based on defaults and input parameters
		KafkaTestConsumerPolicy consumerPolicy = KafkaTestConsumerPolicy.parseFromArgsInput(args);
		// create properties ready to pass to Kafka consumer
		Properties configuration = KafkaTestConsumerConfiguration.getProperties(networkConfiguration, consumerPolicy);

		System.out.println("Start Kafka Simple Data Consumer");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// Create consumer - this class
		KafkaTestDataConsumer kafkaSimpleDataConsumer = new KafkaTestDataConsumer();
		// Start main processing
		kafkaSimpleDataConsumer.work(configuration, consumerPolicy);
	}

	private void work(Properties configuration, KafkaTestConsumerPolicy consumerPolicy) {
		// register Ctrl-C handler
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				latch.countDown();
			}
		});

		// Create Kafka consumer, pass configuration parameters
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(configuration);
		try (consumer) {
			// Connect to the topic and register handler for partitions assignments updates
			consumer.subscribe(Arrays.asList(consumerPolicy.getTopicName()), new ConsumerRebalancer());

			while (latch.getCount() > 0) {
				try {
					// Get records, wait 4sec to get something then end
					ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(4000));
					// Print the content
					if (!records.isEmpty()) {
						records.forEach(record -> {
							System.out.println(KafkaUtils.toString("C", record));
						});
					} else {
						System.out.println("C no new data");
					}
					// Commit the offset in synchronous mode
					consumer.commitSync();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class ConsumerRebalancer implements ConsumerRebalanceListener {
		@Override
		public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
			System.out.println("Repartition rebalance detected. Current state:" + partitions);
		}

		@Override
		public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
			System.out.println("Repartition new assigen detected:" + partitions);
		}
	}
}
