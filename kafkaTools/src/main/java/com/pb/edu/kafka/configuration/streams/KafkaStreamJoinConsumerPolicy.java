package com.pb.edu.kafka.configuration.streams;

import com.pb.edu.kafka.configuration.KafkaOrderProducerPolicy;
import com.pb.edu.kafka.configuration.KafkaProductProducerPolicy;

public class KafkaStreamJoinConsumerPolicy {
	private String productTopicName = KafkaProductProducerPolicy.PRODUCT_TOPIC_NAME;
	private String orderTopicName = KafkaOrderProducerPolicy.ORDER_TOPIC_NAME;
	private String consumerGroup = "consumerJoinGroup";

	public String getOrderTopicName() {
		return orderTopicName;
	}

	public void setOrderTopicName(String orderTopicName) {
		this.orderTopicName = orderTopicName;
	}

	public String getProductTopicName() {
		return productTopicName;
	}

	public void setProductTopicName(String productTopicName) {
		this.productTopicName = productTopicName;
	}

	public String getConsumerGroup() {
		return consumerGroup;
	}

	public void setConsumerGroup(String consumerGroup) {
		this.consumerGroup = consumerGroup;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ConsumerGroup:" + consumerGroup);
		sb.append(", OrderTopic:" + orderTopicName);
		sb.append(", ProductTopic:" + productTopicName);
		return sb.toString();
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	public static String getHelpMessage() {
		return "Expected: Consumer Group Id [as string], Order Topic Name [as string], Product Topic Name [as string]";
	}

	public static KafkaStreamJoinConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaStreamJoinConsumerPolicy consumerConsumingPolicy = new KafkaStreamJoinConsumerPolicy();
		if (args.length > 0) {
			consumerConsumingPolicy.setConsumerGroup(args[0]);
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setOrderTopicName(args[1]);
		}
		if (args.length > 2) {
			consumerConsumingPolicy.setProductTopicName(args[2]);
		}
		return consumerConsumingPolicy;
	}
}
