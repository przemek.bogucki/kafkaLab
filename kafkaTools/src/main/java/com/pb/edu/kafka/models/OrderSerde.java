package com.pb.edu.kafka.models;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class OrderSerde implements Serde<Order> {

	private static OrderSerde instance = new OrderSerde();

	public static OrderSerde getOrderSerde() {
		return instance;
	}

	public OrderSerde() {
	}

	@Override
	public Serializer<Order> serializer() {
		return new ObjectSerializer<Order>();
	}

	@Override
	public Deserializer<Order> deserializer() {
		return new ObjectDeserializer<Order>(Order.class);
	}

}
