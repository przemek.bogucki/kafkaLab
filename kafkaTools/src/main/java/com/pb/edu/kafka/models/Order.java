package com.pb.edu.kafka.models;

import java.io.Serializable;
import java.util.Objects;

import com.pb.edu.kafka.KafkaUtils;

public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	private int orderId;
	private int orderType;
	private int cusotmerId;
	private int productId;
	private long orderTime;
	private String description;

	public Order() {
	}

	public Order(int orderId, int productType, int cusotmerId, int productId, long orderTime) {
		this.orderId = orderId;
		this.orderType = productType;
		this.cusotmerId = cusotmerId;
		this.productId = productId;
		this.orderTime = orderTime;
	}

	public int getOrderId() {
		return orderId;
	}

	public int getOrderType() {
		return orderType;
	}

	public int getCusotmerId() {
		return cusotmerId;
	}

	public int getProductId() {
		return productId;
	}

	public long getOrderTime() {
		return orderTime;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderType=" + orderType + ", cusotmerId=" + cusotmerId + ", productId="
				+ productId + ", orderTime=" + KafkaUtils.timeToString(orderTime) + ", description=" + description
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(cusotmerId, description, orderId, orderTime, orderType, productId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return cusotmerId == other.cusotmerId && Objects.equals(description, other.description)
				&& orderId == other.orderId && orderTime == other.orderTime && orderType == other.orderType
				&& productId == other.productId;
	}

}
