package com.pb.edu.kafka.configuration.streams;

import com.pb.edu.kafka.configuration.KafkaProductProducerPolicy;

public class KafkaStreamProductConsumerPolicy extends KafkaStreamTestConsumerPolicy {
	public KafkaStreamProductConsumerPolicy() {
		setConsumerGroup("productStreamConsumerGroup");
		setTopicName(KafkaProductProducerPolicy.PRODUCT_TOPIC_NAME);
	}
}
