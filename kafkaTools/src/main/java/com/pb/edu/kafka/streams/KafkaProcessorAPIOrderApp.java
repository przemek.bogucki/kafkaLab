package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.ProcessorContext;
import org.apache.kafka.streams.processor.api.Record;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerPolicy;
import com.pb.edu.kafka.models.Order;
import com.pb.edu.kafka.models.PairSerde;

public class KafkaProcessorAPIOrderApp {

	private CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamOrderConsumerPolicy consumerPolicy = KafkaStreamOrderConsumerPolicy.parseFromArgsInput(args);

		consumerPolicy
				.setConsumerGroup(consumerPolicy.getTopicName() + KafkaProcessorAPIOrderApp.class.getCanonicalName());

		KafkaProcessorAPIOrderApp consumer = new KafkaProcessorAPIOrderApp();
		Properties configuration = KafkaStreamOrderConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy, KafkaProcessorAPIOrderApp.class);

		System.out.println("Start Kafka Processor API Consumer");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		consumer.work(configuration, consumerPolicy);
	}

	private void work(Properties configuration, KafkaStreamOrderConsumerPolicy consumerPolicy) {
		// Create in memory key value storage for gathered data
		StoreBuilder<KeyValueStore<String, Pair<Long, Long>>> countStoreSupplier = Stores
				.keyValueStoreBuilder(Stores.inMemoryKeyValueStore("OrderCounts"), Serdes.String(), new PairSerde());

		Topology topology = new Topology();
		// add the source processor node that takes Kafka topic "source-topic" as input
		topology.addSource("Source", consumerPolicy.getTopicName())
				// add the order count processor node which takes the source processor as its
				// upstream processor. It counts number of events for the given product id and
				// prints the results periodically
				.addProcessor("OrdersCountProcessing", () -> new SumProcessor(), "Source")
				// Assign created storage to the OrderCountProcessor processor
				.addStateStore(countStoreSupplier, "OrdersCountProcessing")
				// add next process in the chain that prints every record
				.addProcessor("OrderSumPrinter", () -> new SumPrinter(), "OrdersCountProcessing")
				// add the sink processor node that takes Kafka topic "sink-topic" as output
				// and the OrderSumPrinter node as its upstream processor
				.addSink("Sink", "sumOfOrdersTopic", new StringSerializer(), new StringSerializer(), "OrderSumPrinter");

		// Create the streams executor
		KafkaStreams streams = new KafkaStreams(topology, configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				streams.close();
				latch.countDown();
			}
		});

		// go Go GO
		try {
			streams.start();
			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.exit(0);

	}

	private class SumProcessor implements Processor<String, Order, String, Pair<Long, Long>> {

		private ProcessorContext<String, Pair<Long, Long>> context;
		private KeyValueStore<String, Pair<Long, Long>> countStore;
		private int myPartition = -1;

		@SuppressWarnings("unchecked")
		@Override
		public void init(ProcessorContext<String, Pair<Long, Long>> context) {
			// keep the processor context locally because we need it in punctuate() and
			// commit()
			this.context = context;

			// retrieve the key-value store named "Counts"
			countStore = (KeyValueStore<String, Pair<Long, Long>>) context.getStateStore("OrderCounts");

			// schedule a punctuate() method every 3 second based on stream-time
			// That we will print sum of storage after every 3 sec based on the stream
			// timeline
			// it means that we check the key store every 3 sec
			// we and send newly created event containing the current sum to the next node
			this.context.schedule(Duration.ofSeconds(3), PunctuationType.STREAM_TIME, (timestamp) -> {
				try {
					System.out.println("Task Id:" + context.taskId() + " at:" + KafkaUtils.timeToString(timestamp)
							+ " flush sum from partitionId:" + myPartition);

					KeyValueIterator<String, Pair<Long, Long>> iterator = this.countStore.all();
					while (iterator.hasNext()) {
						// get current value
						KeyValue<String, Pair<Long, Long>> entry = iterator.next();
						// forward to the next node
						context.forward(
								new Record<String, Pair<Long, Long>>(entry.key, entry.value, entry.value.getRight()));

					}

					// commit the current processing progress
					context.commit();
				} catch (Exception e) {
					e.printStackTrace();
					latch.countDown();
				}
			});

		}

		@Override
		public void process(Record<String, Order> order) {
			// processing of received record
			String productId = Integer.toString(order.value().getProductId());
			long sum = countStore.get(productId) != null ? countStore.get(productId).getLeft() : 0;
			// increase a sum of given product id
			countStore.put(productId, Pair.of(Long.valueOf(sum + 1), order.value().getOrderTime()));
			myPartition = context.recordMetadata().get().partition();
		}

		@Override
		public void close() {
			// Auto-generated method stub
		}
	}

	private class SumPrinter implements Processor<String, Pair<Long, Long>, String, String> {

		private ProcessorContext<String, String> context;

		@Override
		public void init(ProcessorContext<String, String> context) {
			this.context = context;
		}

		@Override
		public void process(Record<String, Pair<Long, Long>> record) {
			// Simple record print
			System.out.println("ProductId:" + record.key() + " sum of orders:" + record.value().getLeft() + " at:"
					+ KafkaUtils.timeToString(record.value().getRight()));
			// and forward to the next node
			context.forward(new Record<String, String>(record.key(), record.value().toString(), record.timestamp()));
		}

	}

}
