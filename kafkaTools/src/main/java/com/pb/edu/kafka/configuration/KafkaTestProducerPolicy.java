package com.pb.edu.kafka.configuration;

public class KafkaTestProducerPolicy {

	private String topicName = "testTopic";
	private String keyPrefix = "key";
	private int messagesToSend = 10000;
	private int keyModule = 10;
	private int sendSleep = 500;

	public int getMessagesToSend() {
		return messagesToSend;
	}

	public void setMessagesToSend(int maxIterations) {
		this.messagesToSend = maxIterations;
	}

	public int getKeyModule() {
		return keyModule;
	}

	public void setKeyModule(int keyModule) {
		this.keyModule = keyModule;
	}

	public int getSendSleep() {
		return sendSleep;
	}

	public void setSendSleep(int sendSleep) {
		this.sendSleep = sendSleep;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	private static String getHelpMessage() {
		return "Expected: sendSleepTime(as int), messagesToSend(as int), keyModule(as int), keyPrefix (as String)";
	}

	public static KafkaTestProducerPolicy parseFromInputArgs(String[] args) {
		KafkaTestProducerPolicy sendConfiguration = new KafkaTestProducerPolicy();
		if (args.length > 0) {
			try {

				if (args.length > 0) {
					sendConfiguration.setSendSleep(Integer.parseInt(args[0]));
				}
				if (args.length > 1) {
					sendConfiguration.setMessagesToSend(Integer.parseInt(args[1]));
				}
				if (args.length > 2) {
					sendConfiguration.setKeyModule(Integer.parseInt(args[2]));
				}
				if (args.length > 3) {
					sendConfiguration.setKeyPrefix(args[3]);
				}
			} catch (NumberFormatException nfe) {
				throw new IllegalArgumentException("Incorrect input format. " + getHelpMessage());
			}

		}
		return sendConfiguration;
	}

	@Override
	public String toString() {
		return "KafkaTestProducerPolicy [topicName=" + topicName + ", keyPrefix=" + keyPrefix + ", messagesToSend="
				+ messagesToSend + ", keyModule=" + keyModule + ", sendSleep=" + sendSleep + "]";
	}

}
