package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;

import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamAggregationConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamAggregationConsumerPolicy;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerPolicy;

public class KafkaMyAggregationApp {

	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamAggregationConsumerPolicy consumerPolicy = KafkaStreamAggregationConsumerPolicy
				.parseFromArgsInput(args);

		consumerPolicy.setConsumerGroup(
				consumerPolicy.getTopicName() + KafkaStreamOrderAggregationApp.class.getCanonicalName());

		KafkaMyAggregationApp consumer = new KafkaMyAggregationApp();

		Properties configuration = KafkaStreamAggregationConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka My Aggregation");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				if (consumer.latch != null) {
					consumer.latch.countDown();
				}
				if (consumer.streams != null) {
					consumer.streams.close();
				}
			}
		});

		consumer.work(configuration, consumerPolicy);
	}

	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);

	private void work(Properties configuration, KafkaStreamAggregationConsumerPolicy consumerPolicy)
			throws IOException {
		System.out.println("HELLO STREAMS!");
	}

}
