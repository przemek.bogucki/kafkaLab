package com.pb.edu.kafka.producers;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaTestProducerPolicy;
import com.pb.edu.kafka.configuration.KafkaTestProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;

public class KafkaCompactedTopicProducer extends KafkaTestDataProducer {

	private static final String COMPACTED_TOPC_NAME = "CompactedTopic";

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		if (KafkaTestProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		KafkaTestProducerPolicy sendPolicy = KafkaTestProducerPolicy.parseFromInputArgs(args);
		sendPolicy.setTopicName(COMPACTED_TOPC_NAME);

		KafkaUtils.createCompactedTopic(inputConfiguration, COMPACTED_TOPC_NAME);

		KafkaCompactedTopicProducer producer = new KafkaCompactedTopicProducer();

		Properties configuration = KafkaTestProducerConfiguration.getProperties(inputConfiguration);

		System.out.println("Start Kafka Compacted Topic Producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		producer.work(configuration, sendPolicy, true);
	}

}
