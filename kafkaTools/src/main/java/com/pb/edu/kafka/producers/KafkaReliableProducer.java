package com.pb.edu.kafka.producers;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import com.pb.edu.kafka.configuration.KafkaReliableProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaReliableProducerPolicy;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;

public class KafkaReliableProducer extends KafkaTestDataProducer {
	public static void main(String[] args) throws InterruptedException, ExecutionException {

		if (KafkaReliableProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		KafkaReliableProducerPolicy sendPolicy = KafkaReliableProducerPolicy.parseFromInputArgs(args);
		KafkaTestDataProducer producer = new KafkaTestDataProducer();

		Properties configuration = KafkaReliableProducerConfiguration.getProperties(inputConfiguration, sendPolicy);

		System.out.println("Start Kafka Reliable Producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		producer.work(configuration, sendPolicy, false);
	}
}
