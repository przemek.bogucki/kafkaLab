package com.pb.edu.kafka.configuration;

public class KafkaOrderProducerPolicy {
	public static final String ORDER_TOPIC_NAME = "orderTopic";

	private boolean randomTime = false;
	private String orderTopicName = ORDER_TOPIC_NAME;
	private String orderKeyPrefix = "orderId:";
	private int customerKeyRange = 10;
	private int orderTypeRange = 4;
	private int productIdRange = 4;

	private int sendSleep = 500;
	private int messagesToSend = 10000;

	public boolean isRandomTime() {
		return randomTime;
	}

	public void setRandomTime(boolean randomTime) {
		this.randomTime = randomTime;
	}

	public void setOrderTopicName(String topicName) {
		this.orderTopicName = topicName;
	}

	public void setSendSleep(int sendSleep) {
		this.sendSleep = sendSleep;
	}

	public void setMessagesToSend(int messagesToSend) {
		this.messagesToSend = messagesToSend;
	}

	public String getOrderTopicName() {
		return orderTopicName;
	}

	public String getOrderKeyPrefix() {
		return orderKeyPrefix;
	}

	public int getCustomerKeyRange() {
		return customerKeyRange;
	}

	public int getOrderTypeRange() {
		return orderTypeRange;
	}

	public int getSendSleep() {
		return sendSleep;
	}

	public int getMessagesToSend() {
		return messagesToSend;
	}

	public int getProductIdRange() {
		return productIdRange;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	private static String getHelpMessage() {
		return "Expected: randomTime (false/true), sendSleep (as int), messagesToSend(as int), orderTopicName (as string)";
	}

	public static KafkaOrderProducerPolicy parseFromInputArgs(String[] args) {
		KafkaOrderProducerPolicy sendConfiguration = new KafkaOrderProducerPolicy();
		if (args.length > 0) {
			sendConfiguration.setRandomTime(args[0].equalsIgnoreCase("true"));
		}
		if (args.length > 1) {
			sendConfiguration.setSendSleep(Integer.parseInt(args[1]));
		}

		if (args.length > 2) {
			sendConfiguration.setMessagesToSend(Integer.parseInt(args[2]));
		}
		if (args.length > 3) {
			sendConfiguration.setOrderTopicName(args[3]);
		}
		return sendConfiguration;
	}

	@Override
	public String toString() {
		return "KafkaOrderProducerPolicy [randomTime= " + randomTime + ", sendSleep=" + sendSleep + ", messagesToSend="
				+ messagesToSend + "orderTopicName=" + orderTopicName + "]";
	}
}
