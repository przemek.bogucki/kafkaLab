package com.pb.edu.kafka.configuration.streams;

import com.pb.edu.kafka.configuration.KafkaOrderProducerPolicy;

public class KafkaStreamAggregationConsumerPolicy extends KafkaStreamTestConsumerPolicy {
	public KafkaStreamAggregationConsumerPolicy() {
		setConsumerGroup("aggregationStreamConsumer");
		setTopicName(KafkaOrderProducerPolicy.ORDER_TOPIC_NAME);
	}

	public static KafkaStreamAggregationConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaStreamAggregationConsumerPolicy consumerConsumingPolicy = new KafkaStreamAggregationConsumerPolicy();
		if (args.length > 0) {
			consumerConsumingPolicy.setConsumerGroup(args[0]);
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setTopicName(args[1]);
		}
		return consumerConsumingPolicy;
	}
}