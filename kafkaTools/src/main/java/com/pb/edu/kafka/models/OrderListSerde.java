package com.pb.edu.kafka.models;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class OrderListSerde implements Serde<List<Order>> {

	private static final OrderListSerde _this = new OrderListSerde();

	public static OrderListSerde getListSerde() {
		return _this;
	}

	@Override
	public Deserializer<List<Order>> deserializer() {
		return new Deserializer<List<Order>>() {

			@SuppressWarnings("unchecked")
			@Override
			public List<Order> deserialize(String topic, byte[] data) {
				try {
					ByteArrayInputStream bais = new ByteArrayInputStream(data);
					ObjectInputStream ois = new ObjectInputStream(bais);
					Object obj = ois.readObject();
					return (ArrayList<Order>) obj;
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
					return new ArrayList<>();
				}
			}
		};
	}

	@Override
	public Serializer<List<Order>> serializer() {
		return new Serializer<List<Order>>() {

			@Override
			public byte[] serialize(String topic, List<Order> data) {
				ArrayList<Order> list = new ArrayList<Order>(data);
				try {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ObjectOutputStream oos = new ObjectOutputStream(baos);
					oos.writeObject(list);
					oos.flush();
					return baos.toByteArray();
				} catch (IOException e) {
					e.printStackTrace();
					return new byte[0];
				}
			}
		};
	}

}
