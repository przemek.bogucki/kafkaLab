package com.pb.edu.kafka.configuration;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

public class KafkaBaseNetworkConfiguration {
	private String IP = "127.0.0.1";
	private List<Pair<String, Integer>> brokers = List.of(Pair.of(IP, 9091), Pair.of(IP, 9092), Pair.of(IP, 9093));

	public String getConfiguration() {
		return brokers.stream().map(pair -> pair.getLeft() + ":" + pair.getRight()).collect(Collectors.joining(","));
	}

}
