package com.pb.edu.kafka.configuration;

import org.apache.kafka.common.IsolationLevel;

public class KafkaTestConsumerPolicy {

	private String topicName = "testTopic";
	private String consumerGroup = "testGroup";
	private IsolationLevel isolationLevel = IsolationLevel.READ_UNCOMMITTED;

	public IsolationLevel getIsolationLevel() {
		return isolationLevel;
	}

	public void setIsolationLevel(IsolationLevel isolationLevel) {
		this.isolationLevel = isolationLevel;
	}

	public String getConsumerGroup() {
		return consumerGroup;
	}

	public void setConsumerGroup(String consumerGroup) {
		this.consumerGroup = consumerGroup;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicName() {
		return topicName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" Isolation Level:" + isolationLevel).append(", consumer group:" + consumerGroup)
				.append(", Topic:" + topicName);
		return sb.toString();
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	protected static String getHelpMessage() {
		return "Expected: ReadCommittedOnly [true/false], Consumer Group Id [as string], Topic Name [as string]";
	}

	public static KafkaTestConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaTestConsumerPolicy consumerConsumingPolicy = new KafkaTestConsumerPolicy();
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("true")) {
				consumerConsumingPolicy.setIsolationLevel(IsolationLevel.READ_COMMITTED);
			}
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setConsumerGroup(args[1]);
		}
		if (args.length > 2) {
			consumerConsumingPolicy.setTopicName(args[2]);
		}
		return consumerConsumingPolicy;
	}

}
