package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.Stores;

import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestConsumerPolicy;
import com.pb.edu.kafka.configuration.streams.KafkaStreamTestConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamTestConsumerPolicy;
import com.sun.net.httpserver.HttpServer;

public class KafkaTableTestApp {

	private HttpServer server;
	private ReadOnlyKeyValueStore<String, String> keyValueStore;
	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaTestConsumerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaStreamTestConsumerPolicy consumerPolicy = KafkaStreamTestConsumerPolicy.parseFromArgsInput(args);
		KafkaTableTestApp consumer = new KafkaTableTestApp();

		Properties configuration = KafkaStreamTestConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka Table Consumer");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				consumer.latch.countDown();
				consumer.streams.close();
				consumer.server.stop(0);
			}
		});

		consumer.httpWork();
		consumer.kafkaWork(configuration, consumerPolicy);

	}

	private void kafkaWork(Properties configuration, KafkaStreamTestConsumerPolicy consumerPolicy) {
		// Storage name will be required for unique identification
		String storageName = "KafkaTableConsumerStorage";
		// Initialization of the builder
		StreamsBuilder builder = new StreamsBuilder();
		// define in memory storage (still there will be kafka changelog structure)
		KeyValueBytesStoreSupplier inMemoryValueStore = Stores.inMemoryKeyValueStore(storageName);
		// create storage
		Materialized<String, String, KeyValueStore<Bytes, byte[]>> materialized = Materialized.as(inMemoryValueStore);
		// create table as a topic consumer with storage in memory
		builder.table(consumerPolicy.getTopicName(), materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		try {
			// go Go GG
			streams.start();
			// wait till stream is running
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}
			System.out.println("Stream state:" + streams.state());

			// Get access to storage. It must be query from stream as object create above do
			// not provide read
			keyValueStore = streams
					.store(StoreQueryParameters.fromNameAndType(storageName, QueryableStoreTypes.keyValueStore()));
			// In loop, print all values from table
			while (latch.getCount() > 0) {
				try {
					KeyValueIterator<String, String> iterator = keyValueStore.all();
					try (iterator) {
						System.out.println("Table state at:" + System.currentTimeMillis());
						iterator.forEachRemaining(
								kv -> System.out.println("Storage content C key:" + kv.key + " , msg:" + kv.value));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				Thread.sleep(2500);
			}

			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void httpWork() {
		try {
			int socketNumber = 8000;
			boolean emptySocket = false;
			while (!emptySocket) {
				try {
					ServerSocket serverSocket = new ServerSocket(socketNumber);
					serverSocket.close();
					emptySocket = true;
				} catch (Exception e) {
					socketNumber++;
				}
			}

			server = HttpServer.create(new InetSocketAddress(socketNumber), 0);
			server.createContext("/keys", (httpExchenger) -> {
				httpExchenger.getHttpContext();
				String response = getRespond(httpExchenger.getRequestURI());
				httpExchenger.sendResponseHeaders(200, response.length());
				OutputStream os = httpExchenger.getResponseBody();
				os.write(response.getBytes());
				os.close();
			});
			server.setExecutor(null); // creates a default executor
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private String getRespond(URI path) {
		String[] elements = path.getPath().split("/");
		if (elements.length == 2 && elements[1].equals("keys")) {
			StringBuilder sb = new StringBuilder();
			sb.append("Current table state:\n");
			keyValueStore.all().forEachRemaining(kv -> sb.append("key:" + kv.key + " , value:" + kv.value + "\n"));
			return sb.toString();
		} else if (elements.length >= 3 && elements[1].equals("keys")) {
			String value = keyValueStore.get(elements[2]);
			if (value != null) {
				return "key:" + elements[2] + " , value:" + value;
			} else {
				return "key:" + elements[2] + " Not Found";
			}
		} else {
			return "Incorrect path\n'\\keys' or '\\keys\\{key}' expected";
		}
	}

}
