package com.pb.edu.kafka.producers;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaOrderProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaOrderProducerPolicy;
import com.pb.edu.kafka.configuration.KafkaProductProducerPolicy;
import com.pb.edu.kafka.models.Order;

public class KafkaOrderProducer {

	public static void main(String[] args) {

		if (KafkaProductProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		KafkaOrderProducerPolicy sendPolicy = KafkaOrderProducerPolicy.parseFromInputArgs(args);
		KafkaOrderProducer producer = new KafkaOrderProducer();

		Properties configuration = KafkaOrderProducerConfiguration.getProperties(inputConfiguration);

		System.out.println("Start Kafka Order Producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		producer.work(configuration, sendPolicy);

	}

	public void work(Properties configuration, KafkaOrderProducerPolicy sendPolicy) {
		Producer<String, Order> producer = new KafkaProducer<>(configuration);

		System.out.println(producer.partitionsFor(sendPolicy.getOrderTopicName()));

		int i = 0;

		ThreadLocalRandom random = ThreadLocalRandom.current();

		while (sendPolicy.getMessagesToSend() == 0 || i < sendPolicy.getMessagesToSend()) {
			try {

				long orderTime = sendPolicy.isRandomTime()
						? System.currentTimeMillis() + (RandomUtils.nextLong(0, 20000) - 10000)
						: System.currentTimeMillis();

				Order order = new Order(i, random.nextInt(1, sendPolicy.getOrderTypeRange()),
						random.nextInt(1, sendPolicy.getCustomerKeyRange()),
						random.nextInt(1, sendPolicy.getProductIdRange()), orderTime);

				String key = sendPolicy.getOrderKeyPrefix() + order.getOrderId();
				ProducerRecord<String, Order> record = new ProducerRecord<String, Order>(sendPolicy.getOrderTopicName(),
						null, order.getOrderTime(), key, order);

				record.headers().add("Partition", null);

				producer.send(record, new Callback() {

					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception == null) {
							System.out.println(KafkaUtils.toString(record, metadata));
						} else {
							System.out.println(key + " , " + order + " - ERROR - " + exception);
						}

					}
				});
				Thread.sleep(sendPolicy.getSendSleep());
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
			i++;
		}
		producer.flush();
		producer.close();
		System.out.println("Push done");

	}
}
