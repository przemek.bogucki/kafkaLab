package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.StreamJoined;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamJoinConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamJoinConsumerPolicy;
import com.pb.edu.kafka.models.Order;
import com.pb.edu.kafka.models.OrderSerde;
import com.pb.edu.kafka.models.Product;
import com.pb.edu.kafka.models.ProductSerde;

public class KafkaJoinStreamApp {
	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamJoinConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamJoinConsumerPolicy consumerPolicy = KafkaStreamJoinConsumerPolicy.parseFromArgsInput(args);

		KafkaJoinStreamApp consumer = new KafkaJoinStreamApp();

		Properties configuration = KafkaStreamJoinConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka Join Stream Order and Product");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				consumer.latch.countDown();
				consumer.streams.close();
			}
		});

		consumer.work(configuration, consumerPolicy);
	}

	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);

	private void work(Properties configuration, KafkaStreamJoinConsumerPolicy consumerPolicy) throws IOException {
		// Initialization of the builder
		final StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create the source stream from the given topic
		KStream<String, Order> theOrderStream = builder.stream(consumerPolicy.getOrderTopicName(),
				Consumed.with(Serdes.String(), OrderSerde.getOrderSerde()));
		KStream<String, Product> theProductStream = builder
				.stream(consumerPolicy.getProductTopicName(),
						Consumed.with(Serdes.String(), ProductSerde.getProductSerde()))
				.selectKey((k, p) -> String.valueOf(p.getProductId()));

		KStream<String, Order> orderNewKeyStream = theOrderStream
				.map((k, o) -> KeyValue.pair(String.valueOf(o.getProductId()), o));

		KStream<String, String> joined = orderNewKeyStream.join(theProductStream,
				(o, r) -> " At: " + KafkaUtils.timeToString(o.getOrderTime()) + " order:" + o.getOrderId()
						+ ", product:" + o.getProductId() + ", current price:" + r.getProductPrice(),
				JoinWindows.ofTimeDifferenceWithNoGrace(Duration.ofSeconds(10)),
				StreamJoined.with(Serdes.String(), OrderSerde.getOrderSerde(), ProductSerde.getProductSerde()));
		joined.peek((k, v) -> System.out.println("K:" + k + " V:" + v)).to("joinedoutput",
				Produced.with(Serdes.String(), Serdes.String()));

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});
		System.out.println(topology.describe());

		// go Go GO
		try {
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialisation and to get
			// already initialised local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}
			System.out.println("Stream state:" + streams.state());

			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	};

}