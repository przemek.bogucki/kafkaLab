package com.pb.edu.kafka.producers;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaTestProducerPolicy;

public class KafkaTestDataProducer {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// Process passed arguments to validate and display help message
		if (KafkaTestProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		// Get default network configuration - list of Kafka nodes
		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		// Get send policy based on defaults and input parameters
		KafkaTestProducerPolicy sendPolicy = KafkaTestProducerPolicy.parseFromInputArgs(args);
		// create properties ready to pass to Kafka producer
		Properties configuration = KafkaTestProducerConfiguration.getProperties(inputConfiguration);

		System.out.println("Start Kafka Simple Data Producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		// Create producer - this class
		KafkaTestDataProducer producer = new KafkaTestDataProducer();
		// Start main processing
		producer.work(configuration, sendPolicy, false);
	}

	public void work(Properties configuration, KafkaTestProducerPolicy sendPolicy, boolean nullFirstKey) {
		// Create Kafka producer instance
		// pass configuration
		KafkaProducer<String, String> producer = new KafkaProducer<>(configuration);

		// Print partition metadata for the given topic
		System.out.println(producer.partitionsFor(sendPolicy.getTopicName()));

		// counter for the message id - this is just to print and has informative
		// meaning only
		int i = 0;

		while (sendPolicy.getMessagesToSend() == 0 || i < sendPolicy.getMessagesToSend()) {
			try {
				int keyValue = i % sendPolicy.getKeyModule();
				String key = sendPolicy.getKeyPrefix() + Integer.toString(keyValue);
				String value = nullFirstKey && keyValue == 0 ? null : "Message id: " + Integer.toString(i);
				Header header = new RecordHeader("PC", this.getClass().getSimpleName().getBytes());

				// Create record, pass topic (destination), key and value
				ProducerRecord<String, String> record = new ProducerRecord<String, String>(sendPolicy.getTopicName(),
						key, value);
				// Add header content
				record.headers().add(header);

				// Send (it will end up in the batch) and register callback for results
				producer.send(record, new Callback() {
					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception == null) {
							System.out.println(KafkaUtils.toString(record, metadata));
						} else {
							System.out.println(key + " , " + value + " - ERROR - " + exception);
						}

					}
				});
				Thread.sleep(sendPolicy.getSendSleep());
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
			i++;
		}
		producer.flush();
		producer.close();
		System.out.println("Push done");

	}
}
