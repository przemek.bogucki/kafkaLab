package com.pb.edu.kafka.configuration;

import java.util.concurrent.ThreadLocalRandom;

public class KafkaTransactionalProducerPolicy extends KafkaTestProducerPolicy {
	private int transactionSleepTime = 5000;
	private boolean manualRelease = true;
	private int transactionSize = 10;
	private int transationId = ThreadLocalRandom.current().nextInt(1, 1000);
	private int transactionTimeoutMs = 120000;

	public void setTransationId(int transationId) {
		this.transationId = transationId;
	}

	public String getTransationId() {
		return String.valueOf(transationId);
	}

	public int getTransactionTimeoutMs() {
		return transactionTimeoutMs;
	}

	public void setTransactionTimeoutMs(int transactionTimeoutMs) {
		this.transactionTimeoutMs = transactionTimeoutMs;
	}

	public int getTransactionSleepTime() {
		return transactionSleepTime;
	}

	public boolean isManualRelease() {
		return manualRelease;
	}

	public void setManualRelease(boolean manualRelease) {
		this.manualRelease = manualRelease;
	}

	public void setTransactionSleepTime(int transactionSleepTime) {
		this.transactionSleepTime = transactionSleepTime;
	}

	public void setTransactionSize(int transactionSize) {
		this.transactionSize = transactionSize;
	}

	public int getTransactionSize() {
		return transactionSize;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	private static String getHelpMessage() {
		return "Expected: manualRelease (as true/false) or numerOfAutomaticIterations (as int), transactionSleep(as int), sendSleep(as int), transactionSize (as int), transactionTimeout(as int)";
	}

	public static KafkaTransactionalProducerPolicy parseFromInputArgs(String[] args) {
		KafkaTransactionalProducerPolicy sendConfiguration = new KafkaTransactionalProducerPolicy();
		if (args.length > 0) {
			try {
				if (args[0].equalsIgnoreCase("true") || args[0].equalsIgnoreCase("false")) {
					sendConfiguration.setManualRelease(Boolean.parseBoolean(args[0]));
				} else {
					sendConfiguration.setManualRelease(false);
					sendConfiguration.setMessagesToSend(Integer.parseInt(args[0]));
				}
				if (args.length > 1) {
					sendConfiguration.setTransactionSleepTime(Integer.parseInt(args[1]));
				}
				if (args.length > 2) {
					sendConfiguration.setSendSleep(Integer.parseInt(args[2]));
				}
				if (args.length > 3) {
					sendConfiguration.setTransactionSize(Integer.parseInt(args[3]));
				}
				if (args.length > 4) {
					sendConfiguration.setTransactionTimeoutMs(Integer.parseInt(args[4]));
				}

			} catch (NumberFormatException nfe) {
				throw new IllegalArgumentException("Incorrect input format. " + getHelpMessage());
			}
		}
		return sendConfiguration;
	}

	@Override
	public String toString() {
		return "KafkaTransactionalProducerPolicy [transactionSleepTime=" + transactionSleepTime + ", manualRelease="
				+ manualRelease + ", transactionSize=" + transactionSize + ", transationId=" + transationId
				+ ", transactionTimeoutMs=" + transactionTimeoutMs + "] " + super.toString();
	}

}
