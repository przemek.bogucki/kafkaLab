package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.Stores;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamAggregationConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamAggregationConsumerPolicy;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerPolicy;
import com.pb.edu.kafka.models.Order;
import com.pb.edu.kafka.models.OrderSerde;

public class KafkaStreamOrderAggregationApp {

	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamAggregationConsumerPolicy consumerPolicy = KafkaStreamAggregationConsumerPolicy
				.parseFromArgsInput(args);

		consumerPolicy.setConsumerGroup(
				consumerPolicy.getTopicName() + KafkaStreamOrderAggregationApp.class.getCanonicalName());

		KafkaStreamOrderAggregationApp consumer = new KafkaStreamOrderAggregationApp();

		Properties configuration = KafkaStreamAggregationConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy);

		System.out.println("Start Kafka Order Stream Aggregation");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				consumer.latch.countDown();
				consumer.streams.close();
			}
		});

		consumer.work(configuration, consumerPolicy);
	}

	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);

	private void work(Properties configuration, KafkaStreamAggregationConsumerPolicy consumerPolicy)
			throws IOException {
		// Storage name will be required for unique identification
		String storageName = "orderAggregationStorage";
		// Initialization of the builder
		StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create the source stream from the given topic
		KStream<String, Order> theStream = builder.stream(consumerPolicy.getTopicName(),
				Consumed.with(Serdes.String(), OrderSerde.getOrderSerde()));

		// transform incoming events by grouping them by order type. In result we have
		// number of events with the given order type
		KGroupedStream<Integer, Order> groupedStream = theStream.groupBy((key, order) -> {
			return order.getCusotmerId();
		}, Grouped.with(Serdes.Integer(), OrderSerde.getOrderSerde()));

		// we create in memory storage where aggregated sum of events per product id
		// would be dropped
		// the key is product id, the value is the sum
		KeyValueBytesStoreSupplier inMemoryValueStore = Stores.inMemoryKeyValueStore(storageName);
		Materialized<Integer, Long, KeyValueStore<Bytes, byte[]>> materialized = Materialized
				.<Integer, Long>as(inMemoryValueStore).withKeySerde(Serdes.Integer()).withValueSerde(Serdes.Long());

		// sum grouped events and store them in the store
		// values are stored in table, so the sum per product in override
		// as events are processed

		groupedStream.aggregate(() -> 0L, (key, order, aggregation) -> {
			long ret = aggregation;
			if (order.getOrderType() == 1) {
				ret++;
			}
			return ret;
		}, materialized);
		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});
		System.out.println(topology.describe());

		// go Go GO
		try {
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialization and to get
			// already initialized local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}
			System.out.println("Stream state:" + streams.state());

			// Get access to the storage
			ReadOnlyKeyValueStore<Integer, Long> localStore = streams.store(StoreQueryParameters
					.fromNameAndType(storageName, QueryableStoreTypes.<Integer, Long>keyValueStore()));

			// Iterate to print the table content,sleep to a while
			while (latch.getCount() > 0) {
				System.out.println("Table state at:" + KafkaUtils.timeToString(System.currentTimeMillis()));
				try {
					KeyValueIterator<Integer, Long> all = localStore.all();
					try (all) {
						localStore.all().forEachRemaining(kv -> {
							System.out.println("Storage content C key:" + kv.key + " , count of key:" + kv.value);
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				Thread.sleep(1000);
			}

			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}

	}
}
