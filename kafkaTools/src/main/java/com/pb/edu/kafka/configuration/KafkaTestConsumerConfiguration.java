package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.StreamsConfig;

public class KafkaTestConsumerConfiguration extends KafkaBaseConsumerConfiguration {

	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration,
			KafkaTestConsumerPolicy consumerConfiguration) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG,
				consumerConfiguration.getIsolationLevel().toString().toLowerCase());
		props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerConfiguration.getConsumerGroup());
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, consumerConfiguration.getConsumerGroup());

		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, OffsetResetStrategy.EARLIEST.name().toLowerCase());
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "6002");
		props.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, "6001");
		props.put(CommonClientConfigs.REQUEST_TIMEOUT_MS_CONFIG, "6003");

		props.put(ConsumerConfig.DEFAULT_API_TIMEOUT_MS_CONFIG, "10000");
		props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "20000");

		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getCanonicalName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getCanonicalName());

		return props;

	}

}
