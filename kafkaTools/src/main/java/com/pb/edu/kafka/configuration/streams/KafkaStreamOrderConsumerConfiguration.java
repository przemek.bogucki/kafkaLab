package com.pb.edu.kafka.configuration.streams;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import com.pb.edu.kafka.configuration.KafkaBaseConsumerConfiguration;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.models.OrderSerde;

public class KafkaStreamOrderConsumerConfiguration extends KafkaBaseConsumerConfiguration {
	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration,
			KafkaStreamOrderConsumerPolicy consumerPolicy, Class<?> parentClass) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerPolicy.getConsumerGroup());
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, parentClass.getSimpleName() + consumerPolicy.getConsumerGroup());
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, OrderSerde.class.getCanonicalName());
		props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 2);
		props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG,
				consumerPolicy.isExactlyOnce() ? StreamsConfig.EXACTLY_ONCE_V2 : StreamsConfig.AT_LEAST_ONCE);

		return props;
	}
}
