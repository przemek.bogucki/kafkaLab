package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaTransactionalProducerConfiguration extends KafkaBaseProducerConfiguration {

	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration,
			KafkaTransactionalProducerPolicy sendPolicy) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, sendPolicy.getTransationId());
		props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true);
		props.put(ProducerConfig.RETRIES_CONFIG, 100);
		props.put(ProducerConfig.TRANSACTION_TIMEOUT_CONFIG, sendPolicy.getTransactionTimeoutMs());

		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		return props;
	}

}
