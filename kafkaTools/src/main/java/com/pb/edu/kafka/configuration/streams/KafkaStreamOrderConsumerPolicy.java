package com.pb.edu.kafka.configuration.streams;

import com.pb.edu.kafka.configuration.KafkaOrderProducerPolicy;

public class KafkaStreamOrderConsumerPolicy extends KafkaStreamTestConsumerPolicy {

	private boolean exactlyOnce = false;

	public KafkaStreamOrderConsumerPolicy() {
		setConsumerGroup("orderStreamConsumer");
		setTopicName(KafkaOrderProducerPolicy.ORDER_TOPIC_NAME);
	}

	public void setExactlyOnce(boolean exactlyOnce) {
		this.exactlyOnce = exactlyOnce;
	}

	public boolean isExactlyOnce() {
		return exactlyOnce;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	protected static String getHelpMessage() {
		return "Expected: ExactlyOnce [false/true], Consumer Group Id [as string], Topic Name [as string]";
	}

	public static KafkaStreamOrderConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaStreamOrderConsumerPolicy consumerConsumingPolicy = new KafkaStreamOrderConsumerPolicy();
		if (args.length > 0) {
			consumerConsumingPolicy.setExactlyOnce(Boolean.parseBoolean(args[0]));
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setConsumerGroup(args[1]);
		}
		if (args.length > 2) {
			consumerConsumingPolicy.setTopicName(args[2]);
		}
		return consumerConsumingPolicy;
	}

}
