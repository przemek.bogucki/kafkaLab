package com.pb.edu.kafka.configuration;

public class KafkaProductProducerPolicy {
	public static final String PRODUCT_TOPIC_NAME = "productTopic";

	private boolean randomTime = false;
	private String productTopicName = PRODUCT_TOPIC_NAME;
	private String productKeyPrefix = "productId:";
	private int productIdRange = 4;

	private int sendSleep = 500;
	private int messagesToSend = 10000;

	public boolean isRandomTime() {
		return randomTime;
	}

	public void setRandomTime(boolean randomTime) {
		this.randomTime = randomTime;
	}

	public void setSendSleep(int sendSleep) {
		this.sendSleep = sendSleep;
	}

	public void setMessagesToSend(int messagesToSend) {
		this.messagesToSend = messagesToSend;
	}

	public String getProductTopicName() {
		return productTopicName;
	}

	public void setProductTopicName(String productTopicName) {
		this.productTopicName = productTopicName;
	}

	public String getProductKeyPrefix() {
		return productKeyPrefix;
	}

	public int getProductIdRange() {
		return productIdRange;
	}

	public int getSendSleep() {
		return sendSleep;
	}

	public int getMessagesToSend() {
		return messagesToSend;
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	private static String getHelpMessage() {
		return "Expected: randomTime (false/true), sendSleep (as int), messagesToSend(as int),productTopicName (as string)";
	}

	public static KafkaProductProducerPolicy parseFromInputArgs(String[] args) {
		KafkaProductProducerPolicy sendConfiguration = new KafkaProductProducerPolicy();
		if (args.length > 0) {
			sendConfiguration.setRandomTime(args[0].equalsIgnoreCase("true"));
		}
		if (args.length > 1) {
			sendConfiguration.setSendSleep(Integer.parseInt(args[1]));
		}

		if (args.length > 2) {
			sendConfiguration.setMessagesToSend(Integer.parseInt(args[2]));
		}
		if (args.length > 3) {
			sendConfiguration.setProductTopicName(args[3]);
		}
		return sendConfiguration;
	}

	@Override
	public String toString() {
		return "KafkaOrderProducerPolicy [randomTime= " + randomTime + ", sendSleep=" + sendSleep + ", messagesToSend="
				+ messagesToSend + "productTopicName=" + productTopicName + "]";
	}

}
