package com.pb.edu.kafka.configuration.streams;

public class KafkaStreamTestConsumerPolicy {
	private String topicName = "testTopic";
	private String consumerGroup = "testGroup";

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getConsumerGroup() {
		return consumerGroup;
	}

	public void setConsumerGroup(String consumerGroup) {
		this.consumerGroup = consumerGroup;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ConsumerGroup:" + consumerGroup);
		sb.append(", Topic:" + topicName);
		return sb.toString();
	}

	public static boolean handleHelpRequest(String[] args) {
		if (args.length > 0 && (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-help")
				|| args[0].equalsIgnoreCase("help"))) {
			System.out.println(getHelpMessage());
			return true;
		}
		return false;
	}

	protected static String getHelpMessage() {
		return "Expected: Consumer Group Id [as string], Topic Name [as string]";
	}

	public static KafkaStreamTestConsumerPolicy parseFromArgsInput(String[] args) {
		KafkaStreamTestConsumerPolicy consumerConsumingPolicy = new KafkaStreamTestConsumerPolicy();
		if (args.length > 0) {
			consumerConsumingPolicy.setConsumerGroup(args[0]);
		}
		if (args.length > 1) {
			consumerConsumingPolicy.setTopicName(args[1]);
		}
		return consumerConsumingPolicy;
	}
}