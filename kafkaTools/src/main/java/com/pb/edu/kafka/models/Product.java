package com.pb.edu.kafka.models;

import java.io.Serializable;
import java.util.Objects;

import com.pb.edu.kafka.KafkaUtils;

public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	private int productId;
	private String productName;
	private int productPrice;
	private long creationTime;

	public int getProductId() {
		return productId;
	}

	public int getProductPrice() {
		return productPrice;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public String getProductName() {
		return productName;
	}

	Product() {
	}

	public Product(int productId, String productName, int productPrice, long creationTime) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productPrice = productPrice;
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", creationTime=" + KafkaUtils.timeToString(creationTime) + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(creationTime, productId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return creationTime == other.creationTime && productId == other.productId;
	}

}
