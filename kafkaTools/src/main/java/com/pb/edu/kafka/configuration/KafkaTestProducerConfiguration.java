package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaTestProducerConfiguration extends KafkaBaseProducerConfiguration {

	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG, 15000);
		props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 3000);
		props.put(ProducerConfig.RETRIES_CONFIG, 2);

		return props;
	}

}
