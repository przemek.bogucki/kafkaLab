package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.SessionWindowedKStream;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.SlidingWindows;
import org.apache.kafka.streams.kstream.TimeWindowedKStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlySessionStore;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.apache.kafka.streams.state.SessionBytesStoreSupplier;
import org.apache.kafka.streams.state.SessionStore;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.apache.kafka.streams.state.WindowStore;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaWindowedOrderConsumerPolicy;
import com.pb.edu.kafka.models.Order;
import com.pb.edu.kafka.models.OrderListSerde;
import com.pb.edu.kafka.models.OrderSerde;

public class KafkaStreamOrderWindowedApp {

	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaWindowedOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaWindowedOrderConsumerPolicy consumerPolicy = KafkaWindowedOrderConsumerPolicy.parseFromArgsInput(args);
		consumerPolicy.setConsumerGroup(consumerPolicy.getTopicName()
				+ KafkaStreamOrderWindowedApp.class.getCanonicalName() + "_" + consumerPolicy.getType());

		KafkaStreamOrderWindowedApp consumer = new KafkaStreamOrderWindowedApp();

		Properties configuration = KafkaStreamOrderConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy, KafkaStreamOrderWindowedApp.class);

		System.out.println("Start Kafka Order Stream Windowed");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				consumer.latch.countDown();
				consumer.streams.close();
			}
		});

		switch (consumerPolicy.getType()) {
		case A:
			consumer.workTimingA(configuration, consumerPolicy, false);
			break;
		case B:
			consumer.workSessionB(configuration, consumerPolicy);
			break;
		case C:
			consumer.workSlidingC(configuration, consumerPolicy);
			break;
		case D:
			consumer.workTimingA(configuration, consumerPolicy, true);
			break;
		case E:
			consumer.workSlidingE(configuration, consumerPolicy);
			break;

		}
	}

	private KafkaStreams streams;
	private CountDownLatch latch = new CountDownLatch(1);
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	private void workTimingA(Properties configuration, KafkaWindowedOrderConsumerPolicy consumerPolicy, boolean filter)
			throws IOException {

		// Name of storage
		String storageName = "TimingStorage";

		// Initialisation of the builder
		StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create stream based on Time Window with 10 sec window
		// duration
		KStream<String, Order> theStream = builder
				.stream(consumerPolicy.getTopicName(), Consumed.with(Serdes.String(), OrderSerde.getOrderSerde()))
				.filter((k, o) -> filter ? o.getProductId() == 1 : true);

		TimeWindowedKStream<Integer, Order> timeWindowedStream = theStream.groupBy((key, order) -> {
			return order.getProductId();
		}, Grouped.with(Serdes.Integer(), OrderSerde.getOrderSerde()))
				.windowedBy(TimeWindows.ofSizeAndGrace(Duration.ofSeconds(5), Duration.ofSeconds(60)));

		// Create a local storage with window duration 5 sec
		WindowBytesStoreSupplier inMemoryWindowValueStoreSupplier = Stores.inMemoryWindowStore(storageName,
				Duration.ofSeconds(filter ? 60 : 20), Duration.ofSeconds(5), false);
		Materialized<Integer, Long, WindowStore<Bytes, byte[]>> materialized = Materialized
				.<Integer, Long>as(inMemoryWindowValueStoreSupplier);
		// Materialise the stream output to the local storage
		timeWindowedStream.count(materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});

		// Ready to go
		System.out.println(topology.describe());
		try {
			// Fire stream processing
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialisation and to get
			// already initialised local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}

			System.out.println("Stream state:" + streams.state());

			// Get store in the mode enabling access (read only mode)
			ReadOnlyWindowStore<Integer, Long> localStore = streams.store(StoreQueryParameters
					.fromNameAndType(storageName, QueryableStoreTypes.<Integer, Long>windowStore()));

			// In the loop we get all window from the last 30sec, we print the content
			// (number of given keys in the window)
			// and we sleep for 5 sec
			long pointInTime = System.currentTimeMillis();
			int delta = 30000;
			int sleep = 5000;
			while (latch.getCount() > 0) {
				Thread.sleep(sleep);
				System.out.println("Storage content state at:" + KafkaUtils.timeToString(pointInTime));
				localStore
						.backwardFetchAll(Instant.ofEpochMilli(pointInTime - delta), Instant.ofEpochMilli(pointInTime))
						.forEachRemaining(kv -> {
							System.out.println("key value:" + kv.key.key() + " window:"
									+ KafkaUtils.toString(kv.key.window()) + ", count of key:" + kv.value);
						});
				pointInTime = System.currentTimeMillis();
			}

			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void workSessionB(Properties configuration, KafkaWindowedOrderConsumerPolicy consumerPolicy)
			throws IOException {
		// Name of storage
		String storageName = "SessionStorage";

		// Initialisation of the builder
		StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create stream based on Session Window with 1.5 sec session
		// timeout (inactivity)
		KStream<String, Order> theStream = builder.stream(consumerPolicy.getTopicName());
		SessionWindowedKStream<Integer, Order> sesstionWindowStream = theStream.groupBy((key, order) -> {
			return order.getProductId();
		}, Grouped.with(Serdes.Integer(), OrderSerde.getOrderSerde()))
				.windowedBy(SessionWindows.ofInactivityGapWithNoGrace(Duration.ofMillis(2000)));

		// Create a local storage with window data retention 20 sec
		SessionBytesStoreSupplier inMemoryWindowValueStoreSupplier = Stores.inMemorySessionStore(storageName,
				Duration.ofSeconds(20));
		Materialized<Integer, Long, SessionStore<Bytes, byte[]>> materialized = Materialized
				.<Integer, Long>as(inMemoryWindowValueStoreSupplier);

		// materialized the stream output to the local storage
		sesstionWindowStream.count(materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});

		// Ready to go
		System.out.println(topology.describe());

		try {
			// Fire stream processing
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialisation and to get
			// already initialised local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}

			System.out.println("Stream state:" + streams.state());

			// Get store in the mode enabling access (read only mode)
			ReadOnlySessionStore<Integer, Long> localStore = streams.store(StoreQueryParameters
					.fromNameAndType(storageName, QueryableStoreTypes.<Integer, Long>sessionStore()));

			// In the loop we get all session from the last 30sec, for keys from 0 to 1000
			// we print the content (number of events with the key in the session plus
			// session duration
			// and we sleep for 5 sec
			long pointInTime = System.currentTimeMillis();
			int sleep = 5000;
			int delta = 30000;
			while (latch.getCount() > 0) {
				System.out.println("Sessions content at:" + KafkaUtils.timeToString(pointInTime));
				for (int k = 0; k < 1000; k++) {
					KeyValueIterator<Windowed<Integer>, Long> iterator = localStore.findSessions(k, pointInTime - delta,
							pointInTime);
					try (iterator) {
						iterator.forEachRemaining(kv -> {
							System.out.println("key value:" + kv.key.key() + " session window:"
									+ KafkaUtils.toString(kv.key.window()) + ", number of keys in session:" + kv.value);
						});
					}
				}
				Thread.sleep(sleep);
				pointInTime = System.currentTimeMillis();
			}
			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void workSlidingC(Properties configuration, KafkaWindowedOrderConsumerPolicy consumerPolicy)
			throws IOException {
		// Name of storage
		String storageName = "SlidingStorage";

		// Initialization of the builder
		StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create stream based on Time Window with 10 sec window
		// duration
		KStream<String, Order> theStream = builder.stream(consumerPolicy.getTopicName());
		TimeWindowedKStream<Integer, Order> timeWindowedStream = theStream.groupBy((key, order) -> {
			return order.getProductId();
		}, Grouped.with(Serdes.Integer(), OrderSerde.getOrderSerde()))
				.windowedBy(SlidingWindows.ofTimeDifferenceAndGrace(Duration.ofSeconds(2), Duration.ofSeconds(15)));

		// Create a local storage with window duration 20 sec
		WindowBytesStoreSupplier inMemoryWindowValueStoreSupplier = Stores.inMemoryWindowStore(storageName,
				Duration.ofSeconds(20), Duration.ofSeconds(2), false);
		Materialized<Integer, Long, WindowStore<Bytes, byte[]>> materialized = Materialized
				.<Integer, Long>as(inMemoryWindowValueStoreSupplier);
		// Materialised the stream output to the local storage
		timeWindowedStream.count(materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});

		// Ready to go
		System.out.println(topology.describe());
		// Create console reader
		Scanner in = new Scanner(System.in);
		try (in) {
			// Fire stream processing
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialization and to get
			// already initialized local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}

			System.out.println("Stream state:" + streams.state());

			// Get store in the mode enabling access (read only mode)
			ReadOnlyWindowStore<Integer, Long> localStore = streams.store(StoreQueryParameters
					.fromNameAndType(storageName, QueryableStoreTypes.<Integer, Long>windowStore()));

			// in the loop we get key to investigate and
			// we check number of key in slides in range of 20 sec
			int delta = 20000;

			while (latch.getCount() > 0) {
				// get key for console
				System.out.println("Key to get (int):");
				int theKey = in.nextInt();

				// Display details for the given key in the range from now - delta to now
				long pointInTime = System.currentTimeMillis();
				System.out.println("Storage content - Slides between " + KafkaUtils.timeToString(pointInTime - delta)
						+ " and " + KafkaUtils.timeToString(pointInTime));
				localStore.fetch(theKey, Instant.ofEpochMilli(pointInTime - delta), Instant.ofEpochMilli(pointInTime))
						.forEachRemaining(kv -> {
							System.out.println("key value:" + theKey + " window:" + KafkaUtils.timeToString(kv.key)
									+ ", count of key:" + kv.value);
						});
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void workSlidingE(Properties configuration, KafkaWindowedOrderConsumerPolicy consumerPolicy)
			throws IOException {
		String storageName = "windowGroupStorage";
		// Initialisation of the builder
		StreamsBuilder builder = new StreamsBuilder();

		final AtomicLong totalInputCount = new AtomicLong(0);
		final AtomicLong totalOutCount = new AtomicLong(0);
		final Duration WINDOW_SIZE = Duration.ofSeconds(10);
		final Duration GRACE_TIME = Duration.ofSeconds(30);
		final Duration RETENTION_TIME = Duration.ofSeconds(180);

		WindowBytesStoreSupplier inMemoryWindowValueStoreSupplier = Stores.inMemoryWindowStore(storageName,
				RETENTION_TIME, WINDOW_SIZE, false);
		Materialized<Integer, List<Order>, WindowStore<Bytes, byte[]>> materialized = Materialized
				.<Integer, List<Order>>as(inMemoryWindowValueStoreSupplier).withCachingDisabled()
				.withKeySerde(Serdes.Integer()).withValueSerde(OrderListSerde.getListSerde())
				.withRetention(RETENTION_TIME);

		// In the Builder, create stream based on Time Window with 10 sec window
		// duration
		KStream<String, Order> theStream = builder.stream(consumerPolicy.getTopicName());

		KStream<Integer, List<Order>> theListStream = theStream.peek((k, v) -> {
			totalInputCount.incrementAndGet();
		}).map((k, v) -> {
			List<Order> list = new ArrayList<Order>();
			list.add(v);
			return new KeyValue<Integer, List<Order>>(v.getProductId(), list);
		});

		theListStream.groupByKey(Grouped.with(Serdes.Integer(), OrderListSerde.getListSerde()))
				.windowedBy(TimeWindows.ofSizeAndGrace(WINDOW_SIZE, GRACE_TIME))
				.aggregate(() -> new ArrayList<Order>(), (k, v, a) -> {
					a.addAll(v);
					a.sort((l1, l2) -> {
						Timestamp t1 = Timestamp.from(Instant.ofEpochMilli(l1.getOrderTime()));
						Timestamp t2 = Timestamp.from(Instant.ofEpochMilli(l2.getOrderTime()));
						return t1.compareTo(t2);
					});
					return a;
				}, materialized);

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});

		// Ready to go
		System.out.println(topology.describe());
		try {
			// Fire stream processing
			streams.start();

			// Here we wait till a stream processing is up and running
			// This is required to avoid race condition with initialisation and to get
			// already initialised local story
			while (streams.state() != State.RUNNING && streams.state() != State.ERROR
					&& streams.state() != State.PENDING_ERROR) {
				Thread.sleep(50);
			}

			// Get store in the mode enabling access (read only mode)
			ReadOnlyWindowStore<Integer, List<Order>> localStore = streams.store(StoreQueryParameters
					.fromNameAndType(storageName, QueryableStoreTypes.<Integer, List<Order>>windowStore()));
			// Get store in the mode enabling access (read only mode)

			scheduler.scheduleWithFixedDelay(() -> {
				long timeNow = System.currentTimeMillis();
				System.out.println("Table content at:" + KafkaUtils.timeToString(timeNow));
				System.out.println("IN:" + totalInputCount.get() + " OUT:" + totalOutCount.get());
				KeyValueIterator<Windowed<Integer>, List<Order>> iterator = localStore.all();
				iterator.forEachRemaining(kv -> {
					System.out.println(KafkaUtils.toString(kv.key) + " value:" + KafkaUtils.toTimeString(kv.value));
				});
				iterator.close();
			}, 0, 10, TimeUnit.SECONDS);
			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

}
