package com.pb.edu.kafka.producers;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.ProducerFencedException;

import com.pb.edu.kafka.KafkaUtils;
import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.KafkaTransactionalProducerConfiguration;
import com.pb.edu.kafka.configuration.KafkaTransactionalProducerPolicy;

public class KafkaTransactionalProducer {

	private CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) {

		if (KafkaTransactionalProducerPolicy.handleHelpRequest(args)) {
			return;
		}

		KafkaBaseNetworkConfiguration inputConfiguration = new KafkaBaseNetworkConfiguration();
		KafkaTransactionalProducerPolicy sendPolicy = KafkaTransactionalProducerPolicy.parseFromInputArgs(args);
		KafkaTransactionalProducer producer = new KafkaTransactionalProducer();

		Properties configuration = KafkaTransactionalProducerConfiguration.getProperties(inputConfiguration,
				sendPolicy);

		System.out.println("Start Transactional Kafka producer");
		System.out.println("Send Policy:" + sendPolicy);
		System.out.println("Configuration:" + configuration);

		producer.work(configuration, sendPolicy);
	}

	public void work(Properties configuration, KafkaTransactionalProducerPolicy sendPolicy) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

		Producer<String, String> producer = new KafkaProducer<>(configuration);
		Scanner in = new Scanner(System.in);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				latch.countDown();
			}
		});

		try (producer; in) {
			producer.initTransactions();
			System.out.println("Transaction mode initialized");

			int i = 0;

			final List<Future<RecordMetadata>> futureList = new ArrayList<Future<RecordMetadata>>();

			while (latch.getCount() > 0
					&& (sendPolicy.getMessagesToSend() == 0 || i < sendPolicy.getMessagesToSend())) {
				try {
					producer.beginTransaction();
					System.out.println("Transaction began");

					for (int tsize = 0; tsize < sendPolicy.getTransactionSize(); tsize++) {
						String key = sendPolicy.getKeyPrefix() + Integer.toString(i % sendPolicy.getKeyModule());
						String message = "Message id: " + Integer.toString(i);

						ProducerRecord<String, String> record = new ProducerRecord<>(sendPolicy.getTopicName(), key,
								message);
						futureList.add(producer.send(record, (RecordMetadata metadata, Exception exception) -> {
							if (exception == null) {
								System.out.println(KafkaUtils.toString(record, metadata));
							} else {
								System.out.println(key + " , " + message + " - ERROR - " + exception);
							}

						}));
						i++;
						Thread.sleep(sendPolicy.getSendSleep());
					}

					futureList.forEach(predicte -> {
						try {
							predicte.get();
						} catch (Exception e) {
							e.printStackTrace();
						}
					});

					if (sendPolicy.isManualRelease()) {
						// ask for transaction commit or reject
						System.out
								.println("(Transaction started at: " + simpleDateFormat.format(Date.from(Instant.now()))
										+ ") - transaction timeout " + sendPolicy.getTransactionTimeoutMs());
						System.out.println("Shall Transaction be committed y/n?");
						System.out.flush();

						String respond = (in.hasNext()) ? in.nextLine() : "n";

						if (respond.equalsIgnoreCase("y") || respond.length() == 0) {
							producer.commitTransaction();
							System.out.println("Transaction committed");
						} else {
							producer.abortTransaction();
							System.out.println("Transaction aborted");
						}

					} else {
						// Automatically commit after sleep time
						System.out.println("Wait " + sendPolicy.getTransactionSleepTime() + " millisec to commit");
						System.out.flush();
						Thread.sleep(sendPolicy.getTransactionSleepTime());
						producer.commitTransaction();
						System.out.println("Transaction committed");
					}

				} catch (Exception e) {
					if (e instanceof ProducerFencedException) {
						System.err.println(simpleDateFormat.format(Date.from(Instant.now()))
								+ " - Transaction expired because of timeout or other producer with the same transactionalId");
						break;
					}
					e.printStackTrace();
					if (latch.getCount() == 0) {
						break;
					}
				}
			}
		} finally {
			producer.abortTransaction();
		}
	}
}
