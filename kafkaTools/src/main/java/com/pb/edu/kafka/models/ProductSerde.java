package com.pb.edu.kafka.models;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class ProductSerde implements Serde<Product> {

	private static ProductSerde instance = new ProductSerde();

	public static ProductSerde getProductSerde() {
		return instance;
	}

	public ProductSerde() {

	}

	@Override
	public Serializer<Product> serializer() {
		return new ObjectSerializer<Product>();
	}

	@Override
	public Deserializer<Product> deserializer() {
		return new ObjectDeserializer<Product>(Product.class);
	}

}
