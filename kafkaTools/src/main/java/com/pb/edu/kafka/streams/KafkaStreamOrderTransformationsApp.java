package com.pb.edu.kafka.streams;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse;
import org.apache.kafka.streams.kstream.Branched;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Named;

import com.pb.edu.kafka.configuration.KafkaBaseNetworkConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerConfiguration;
import com.pb.edu.kafka.configuration.streams.KafkaStreamOrderConsumerPolicy;
import com.pb.edu.kafka.models.Order;

public class KafkaStreamOrderTransformationsApp {

	public static void main(String[] args) throws IOException {
		KafkaBaseNetworkConfiguration networkConfiguration = new KafkaBaseNetworkConfiguration();

		if (KafkaStreamOrderConsumerPolicy.handleHelpRequest(args)) {
			return;
		}
		KafkaStreamOrderConsumerPolicy consumerPolicy = KafkaStreamOrderConsumerPolicy.parseFromArgsInput(args);

		consumerPolicy.setConsumerGroup(
				consumerPolicy.getTopicName() + KafkaStreamOrderTransformationsApp.class.getCanonicalName());

		KafkaStreamOrderTransformationsApp consumer = new KafkaStreamOrderTransformationsApp();

		Properties configuration = KafkaStreamOrderConsumerConfiguration.getProperties(networkConfiguration,
				consumerPolicy, KafkaStreamOrderTransformationsApp.class);

		System.out.println("Start Kafka Order Stream Transformation");
		System.out.println("Consumer Policy:" + consumerPolicy);
		System.out.println("Configuration:" + configuration);

		consumer.work(configuration, consumerPolicy);
	}

	private void work(Properties configuration, KafkaStreamOrderConsumerPolicy consumerPolicy) throws IOException {
		// Initialization of the builder
		final StreamsBuilder builder = new StreamsBuilder();

		// In the Builder, create the source stream from the given topic
		KStream<String, Order> theStream = builder.stream(consumerPolicy.getTopicName());
		// For each incoming event make a print to stout
		theStream.foreach((k, o) -> System.out
				.println("In thread:" + Thread.currentThread().getId() + " Key:" + k + " order:" + o));

		// branch stream to three following streams based on product type
		Map<String, KStream<String, Order>> branchedStreams = theStream.split(Named.as("ProductType-"))
				.branch((key, order) -> order.getProductId() == 1, Branched.as("1"))
				.branch((key, order) -> order.getProductId() == 2, Branched.as("2")).defaultBranch(Branched.as("rest"));

		// On each of branch, map source event to a new one with the other key,
		// CustomerID
		// then print the output result
		for (String mapKey : branchedStreams.keySet()) {
			KStream<String, Order> subStream = branchedStreams.get(mapKey);
			subStream.map((ok, ov) -> {
				return new KeyValue<String, Order>("CustomerId-" + ov.getCusotmerId(), ov);
			}).peek((k, v) -> System.out.println("		forward to Topic:" + mapKey + " order:" + v)).to(mapKey);
		}
		// In result we haves split incoming stream to three per product id and then we
		// track events based on user id

		// Build topology, with builder in which a the stream processing is defined
		// register handler to shutdown in case of error
		Topology topology = builder.build();
		KafkaStreams streams = new KafkaStreams(topology, configuration);
		streams.setUncaughtExceptionHandler(e -> {
			e.printStackTrace();
			return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
		});
		System.out.println("Processing topology" + topology.describe());

		// attach shutdown handler to catch control-c
		CountDownLatch latch = new CountDownLatch(1);
		Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
			@Override
			public void run() {
				System.out.println("Exit handled");
				streams.close();
				latch.countDown();
			}
		});

		// Wait to start
		System.out.println("Press to start...");
		System.in.read();

		// go Go GO
		try {
			streams.start();
			latch.await();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.exit(0);

	}

}
