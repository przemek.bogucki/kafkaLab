package com.pb.edu.kafka.configuration;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import com.pb.edu.kafka.models.ProductSerializer;

public class KafkaProductProducerConfiguration extends KafkaBaseProducerConfiguration {
	public static Properties getProperties(KafkaBaseNetworkConfiguration inputConfiguration) {
		Properties props = getBaseProperties(inputConfiguration);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ProductSerializer.class.getCanonicalName());
		return props;
	}
}
