package com.pb.edu.kafka.models;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectDeserializer<E> implements Deserializer<E> {

	private Class<E> valueType;

	public ObjectDeserializer(Class<E> valueType) {
		this.valueType = valueType;
	}

	private static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public E deserialize(String topic, byte[] data) {
		E object = null;
		try {
			object = objectMapper.readValue(data, valueType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;

	}

}
