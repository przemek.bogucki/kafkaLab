set IP_INTERFACE_ALIAS 'Ethernet'  #It must be updated to reflect existing network interface
set MACHINE_IP (Get-NetIPConfiguration -InterfaceAlias $IP_INTERFACE_ALIAS).IPv4Address.IPAddress

if ($MACHINE_IP -match "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"){
	echo ("Found IP:" + $MACHINE_IP)
	set command1 "Set-Variable -Name ""MACHINE_IP"" -Value $MACHINE_IP -Scope global; Write-Output ""MACHINE_IP:"" ; Write-Output $MACHINE_IP"
	set command2 " ; docker rm -f (docker ps -aq) ; Write-Output 'All docekers removed'"
	
	set commandz " ; docker run --name zookeeper -p 2181:2181 -p 2888:2888 -p 3888:3888 -d przemekbogucki/lab_repo:zookeeper_3.6.3 ; Write-Output 'Zookeper started wait 5sec'"
	
	set commands " ; Start-Sleep 5"
	
	set commandk " ; docker run --name kafka1 -e KAFKA_LISTENERS=PLAINTEXT://:9091 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://""$MACHINE_IP"":9091 -e KAFKA_BROKER_ID=1 -e ZOOKEEPER_CONNECTION_STRING=""$MACHINE_IP"":2181 -e KAFKA_DELETE_TOPIC_ENABLE=true -e JMX_PORT=9991 -p 9991:9991 -p 9091:9091 -d przemekbogucki/lab_repo:kafka_2.7.2 ; Write-Output 'Kafka1 started'`
	               ; docker run --name kafka2 -e KAFKA_LISTENERS=PLAINTEXT://:9092 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://""$MACHINE_IP"":9092 -e KAFKA_BROKER_ID=2 -e ZOOKEEPER_CONNECTION_STRING=""$MACHINE_IP"":2181 -e KAFKA_DELETE_TOPIC_ENABLE=true -e JMX_PORT=9992 -p 9992:9992 -p 9092:9092 -d przemekbogucki/lab_repo:kafka_2.7.2 ; Write-Output 'Kafka2 started'`
				   ; docker run --name kafka3 -e KAFKA_LISTENERS=PLAINTEXT://:9093 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://""$MACHINE_IP"":9093 -e KAFKA_BROKER_ID=3 -e ZOOKEEPER_CONNECTION_STRING=""$MACHINE_IP"":2181 -e KAFKA_DELETE_TOPIC_ENABLE=true -e JMX_PORT=9993 -p 9993:9993 -p 9093:9093 -d przemekbogucki/lab_repo:kafka_2.7.2 ; Write-Output 'Kafka3 started'"
	set commandCM " ; docker run --name cmak -p 9000:9000 -e ZK_HOSTS=$MACHINE_IP"":2181"" -e APPLICATION_SECRET=tcuser -d przemekbogucki/lab_repo:cmak_3.0.0.5 ; Write-Output 'cmak started'"

	set commandPU " ; docker run --name pandaUi -p 8080:8080 -e KAFKA_BROKERS=host.docker.internal:9092 -d docker.redpanda.com/redpandadata/console:latest ; Write-Output 'redPanda started'" 	
	
	$command = $command1 + $command2 + $commandz + $commands + $commandk + $commandCM + $commandPU
	Start-Process powershell -WorkingDirectory "C:\" -Verb runas -ArgumentList "-NoExit -Command $command "
}else{
	echo "Network Interface" + $IP_INTERFACE_ALIAS + " not found. Check avaliable interfaces with ipconfig and update IP_INTERFACE_ALIAS value in this script"
	Write-Host -NoNewLine 'Press any key to continue...';
}
